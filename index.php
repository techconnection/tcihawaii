<?php 

require_once str_replace('\\', '/', $_SERVER['DOCUMENT_ROOT']) . '/system/config/config.php';

if (is_file(ROOT_DIR . '/install/startup.php')) {
    require_once ROOT_DIR . '/install/startup.php';
} else {
    require_once ROOT_DIR . '/system/startup.php';
}