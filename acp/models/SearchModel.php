<?php 

/**
 * Search Model Class
 *
 * This class interact with the database and tries to find data that matches 
 * the users search string.
 */
class SearchModel extends Model
{
    public function searchUsers($data)
    {
        $array = [];
        $query = $this->con->mysqli->query('SELECT * FROM `users` WHERE `username` LIKE "%'. $data . '%"');
        while ($row = $query->fetch_assoc()) {
            $array[] = $row;
        }
        $query->free();
        return $array;
    }

    public function searchBlogPosts($data)
    {
        $array = [];
        $query = $this->con->mysqli->query('SELECT * FROM `blog` WHERE `title` LIKE "%'. $data . '%"');
        while ($row = $query->fetch_assoc()) {
            $array[] = $row;
        }
        $query->free();
        return $array;
    }

    public function searchPages($data)
    {
        $array = [];
        $query = $this->con->mysqli->query('SELECT * FROM `pages` WHERE `name` LIKE "%'. $data . '%"');
        while ($row = $query->fetch_assoc()) {
            $array[] = $row;
        }
        $query->free();
        return $array;
    }
}