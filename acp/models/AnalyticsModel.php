<?php 

class AnalyticsModel extends Model
{
    public function insertAnalyticsCode($code)
    {
        $query = $this->con->mysqli->prepare('INSERT INTO analytics (`code`) VALUES(?)');
        $query->bind_param('s', $code);
        if ($query->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function getAnalyticsCode()
    {
        $query = $this->con->mysqli->query('SELECT * FROM `analytics`');
        $array = $query->fetch_assoc();
        return $array;
    }

    public function updateAnalyticsCode($code, $analytics_id = 1)
    {
        $query = $con->mysqli->prepare('UPDATE `analytics` SET `code` = ? WHERE `analytics_id` = ?');
        $query->bind_param('ss', $code, $analytics_id);
        if ($query->execute()) {
            return true;
        } else {
            return false;
        }
    }
}
