<?php 

/**
 * Settings Model Class
 *
 * Interact with the database to process data related to the settings.
 */
class SettingsModel extends Model
{
    public function getSetting($data)
    {
        $query = $this->con->mysqli->query('SELECT `value` FROM `settings` WHERE `name` = "' . $data . '"');
        $array = $query->fetch_assoc();  
        $value = $array['value'];
        return $value;
    }

    public function updateSetting($param, $id)
    {
        $query = $this->con->mysqli->prepare('UPDATE `settings` SET `value` = ? WHERE `id` = ?');
        $query->bind_param('ss', $param, $id);
        if ($query->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function getMailSettings()
    {
        $query = $this->con->mysqli->query('SELECT * FROM `mail`');
        $array = $query->fetch_assoc();  
        return $array;
    }

    public function updateMailSettings($record, $data)
    {
        $query = $this->con->mysqli->prepare('UPDATE `mail` SET `' . $record . '` = ?');
        $query->bind_param('s', $data);
        if ($query->execute()) {
            return true;
        } else {
            return false;
        }
    }
}