<?php

/**
 * Newsletter Model Class
 *
 * Interact with the database to process data related to accounts.
 */
class NewsletterModel extends Model
{
    public function getSubscribers($row, $data)
    {
        $array = [];
        $query = $this->con->mysqli->query('SELECT * FROM `newsletter` WHERE ' . $row . ' = "' . $data . '"');
        while ($row = $query->fetch_assoc()) {
            $array[] = $row;
        }
        $query->free();
        return $array;
    }


    public function getSubscriber($param, $data)
    {
        $query = $this->con->mysqli->query('SELECT * FROM `newsletter` WHERE `' . $param . '` = "' . $data . '"');
        $array = $query->fetch_assoc();
        return $array;
    }

    public function insertSubscriber($email)
    {
        $query = $this->con->mysqli->prepare('INSERT INTO `newsletter` (email) VALUES(?)');
        $query->bind_param('s', $email);
        if ($query->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function updateSubscription($record, $data, $email)
    {
        $query = $this->con->mysqli->prepare('UPDATE `newsletter` SET `' . $record . '` = ? WHERE `email` = ?');
        $query->bind_param('ss', $data, $email);
        if ($query->execute()) {
            if ($query->affected_rows > 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function getTotalSubscriptionsNumber()
    {
        $query = $this->con->mysqli->query('SELECT * FROM `newsletter` WHERE `status` = "1"');
        $num = $query->num_rows;
        return $num;
    }

    public function getLastSubscription()
    {
        $query = $this->con->mysqli->query('SELECT * FROM `newsletter` ORDER BY `date_subscribed` DESC LIMIT 1');
        $array = $query->fetch_assoc();
        return $array;
    }
}