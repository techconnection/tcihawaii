<?php 

/**
 * Pages Model
 *
 *
 * 
 *
 */
class PagesModel extends Model
{
    public function getPageById($data)
    {
        $query = $this->con->mysqli->query('SELECT * FROM `pages` WHERE `id` = "' . $data . '"');
        $array = $query->fetch_assoc();  
        return $array;
    }

    public function getPage($data)
    {
        $query = $this->con->mysqli->query('SELECT * FROM `pages` WHERE `name` = "' . $data . '"');
        $array = $query->fetch_assoc();  
        return $array;
    }

    public function getPageContent($data)
    {
        $query = $this->con->mysqli->query('SELECT `content` FROM `pages` WHERE `name` = "' . $data . '"');
        $array = $query->fetch_assoc();
        $content = $array['content'];
        return $content;
    }

    public function insertPage($title, $name, $route, $view, $controller)
    {
        $time = date('c');
        $query = $this->con->mysqli->prepare('INSERT INTO `pages` (title, name, route, view_file, controller_file, last_edit) VALUES(?, ?, ?, ?, ?, ?)');
        $query->bind_param('ssssss', $title, $name, $route, $view, $controller, $time);
        if ($query->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function updatePage($record, $content, $id)
    {
        $time = date('c');
        $query = $this->con->mysqli->prepare('UPDATE `pages` SET `' . $record . '` = ?, `last_edit` = ? WHERE `name` = ?');
        $query->bind_param('sss', $content, $time, $id);
        if ($query->execute()) {
            if ($query->affected_rows > 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function deletePage($id)
    {
        $delete = $this->con->mysqli->prepare('DELETE FROM `pages` WHERE `id` = ?');
        $delete->bind_param('s', $id);
        if ($delete->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function eraseSitemap()
    {
        $query = $this->con->mysqli->query('TRUNCATE `sitemap`');
        return $query;
    }

    public function getPageName($data)
    {
        $query = $this->con->mysqli->query('SELECT `name` FROM `pages` WHERE `name` = "' . $data . '"');
        $array = $query->fetch_row();  
        return $array;
    }

    public function insertSitelink($name, $route_name, $route, $link_order)
    {
        $query = $this->con->mysqli->prepare('INSERT INTO `sitemap` (`name`, `route_name`, `route`, `link_order`) VALUES(?, ?, ?, ?)');
        $query->bind_param('ssss', $name, $route_name, $route, $link_order);
        if ($query->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function getSitelinks()
    {
        $array = [];
        $query = $this->con->mysqli->query('SELECT * FROM `sitemap` ORDER BY `link_order` ASC');
        while ($row = $query->fetch_assoc()) {
            $array[] = $row;
        }
        $query->free();
        return $array;
    }

    public function getSitelink($name)
    {
        $query = $this->con->mysqli->query('SELECT * FROM `sitemap` WHERE `name` = "' . $name . '"');
        $array = $query->fetch_assoc();  
        if ($array != NULL) {
            return true;
        } else {
            return false;
        }
    }

    public function updateSitemap($record, $data, $name)
    {
        $query = $this->con->mysqli->prepare('UPDATE `sitemap` SET `' . $record . '` = ? WHERE `name` = ?');
        $query->bind_param('ss', $data, $name);
        if ($query->execute()) {
            if ($query->affected_rows > 0) {
                return true;
            } else {
                return false;
            }
        }
    }




    public function getTotalPageNumber()
    {
        $result = $this->con->mysqli->query('SELECT * FROM `pages`');
        $num = mysqli_num_rows($result);
        return $num;
    }

    public function getMostViewed()
    {
        $query = $this->con->mysqli->query('SELECT * FROM `pages` WHERE `views` = (SELECT max(views) FROM `pages`)');
        $array = $query->fetch_assoc();
        return $array;
    }

    public function getTotalPageViews()
    {
        $query = $this->con->mysqli->query('SELECT * FROM `pages`');
        $num = 0;
        while ($row = $query->fetch_assoc()) {
            $array[] = $row;
        }
        foreach ($array as $a) {
            $num = $a['views'] + $num;
        }
        return $num;
    }
}
