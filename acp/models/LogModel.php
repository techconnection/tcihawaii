<?php 

/**
 * Log Model Class
 *
 * Interact with the database to process data related to the log.
 */
class LogModel extends Model
{
    public function getLog()
    {
        $query = [];
        $query = $this->con->mysqli->query('SELECT * FROM `log` ORDER by `id` DESC');
        while ($row = $query->fetch_assoc()) {
            $array[] = $row;
        }
        $query->free();
        if (isset($array)) {
            return $array;
        }
    }

    public function insertLog($event)
    {
        $query = $this->con->mysqli->prepare('INSERT INTO log (`event`) VALUES(?)');
        $query->bind_param('s', $event);
        if ($query->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function clearLog()
    {
        if ($this->con->mysqli->query('TRUNCATE TABLE `log`')) {
            return true;
        } else {
            return false;
        }
    }
}