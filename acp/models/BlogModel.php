<?php 

/**
 * Blog Model Class
 *
 */
class BlogModel extends Model
{
    public function getPosts($limit)
    {
        $query = [];
        if ($result = $this->con->mysqli->query('SELECT * FROM `blog` ORDER BY `id` DESC LIMIT ' . $limit)) {
            while ($array = $result->fetch_assoc()) {
                $query[] = $array;
            }
            $result->free();
        }
        return $query;
    }

    public function getPost($param, $data)
    {
        $query = $this->con->mysqli->query('SELECT * FROM `blog` WHERE `' . $param . '` = "' . $data . '"');
        $array = $query->fetch_assoc();
        return $array;
    }

    public function insertPost($author, $title, $body, $preview_image)
    {
        $insert = $this->con->mysqli->prepare('INSERT INTO `blog` (`author`, `title`, `body`, `preview_image`) VALUES(?, ?, ?, ?)');
        $insert->bind_param('ssss', $author, $title, $body, $preview_image);
        if ($insert->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function updateBlog($title, $body, $preview_image, $update_id)
    {
        $time = date('c');
        $query = $this->con->mysqli->prepare('UPDATE `blog` SET `title` = ?, `body` = ?, `preview_image` = ?, `last_edit` = "' . $time . '" WHERE `id` = ?');
        $query->bind_param('ssss', $title, $body, $preview_image, $update_id);
        if ($query->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function deletePost($id)
    {
        $delete = $this->con->mysqli->prepare('DELETE FROM `blog` WHERE `id` = ?');
        $delete->bind_param('s', $id);
        if ($delete->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function getTotalPostsNumber()
    {
        $result = $this->con->mysqli->query('SELECT * FROM `blog`');
        $num = mysqli_num_rows($result);
        return $num;
    }

    public function getViewsNumber()
    {
        $query = [];
        $result = $this->con->mysqli->query('SELECT * FROM `blog`');
        $number = 0;
        foreach ($result as $r) {
            $number = $number + $r['views'];
        }
        $result->free();
        return $number;
    }

    public function getMostViewed()
    {
        $query = $this->con->mysqli->query('SELECT * FROM `blog` WHERE `views` = (SELECT max(views) FROM `blog`)');
        $array = $query->fetch_assoc();
        return $array;
    }

    public function getLastPost()
    {
        $query = $this->con->mysqli->query('SELECT * FROM `blog` ORDER BY `date_posted` DESC LIMIT 1');
        $array = $query->fetch_assoc();
        return $array;
    }

    public function getLastEdited()
    {
        $query = $this->con->mysqli->query('SELECT * FROM `blog` ORDER BY `last_edit` DESC LIMIT 1');
        $array = $query->fetch_assoc();
        return $array;
    }
}
