<?php 

/**
 * User Model Class
 *
 * Interact with the database to process data related to the users.
 */
class UserModel extends Model
{
    /**
     * Get a single users data
     *
     * Get a users data and return their data in an array.
     * @param $param
     * @param $data  
     * @return array
     */
    public function getUser($param, $data)
    {
        $query = $this->con->mysqli->query('SELECT * FROM `users` WHERE `' . $param . '` = "' . $data . '"');
        $array = $query->fetch_assoc();
        return $array;
    }

    /**
     * Insert user into database
     *
     * Insert a new user record into the database.
     * @param string $username    
     * @param string $email       
     * @param string $password    
     * @param string $key         
     * @param string $last_active 
     * @param string $ip      
     * @return bool   
     */
    public function insertUser($username, $email, $password, $key, $last_active, $ip)
    {
        $query = $this->con->mysqli->prepare('INSERT INTO `users` (`username`, `email`, `password`, `key`, `last_active`, `ip`) VALUES(?, ?, ?, ?, ?, ?)');
        $query->bind_param('ssssss', $username, $email, $password, $key, $last_active, $ip);
        if ($query->execute()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Update a user record
     *
     * @param string $record 
     * @param mixed $data   
     * @param int $id     
     * @return bool         
     */
    public function updateUser($record, $data, $id)
    {
        $query = $this->con->mysqli->prepare('UPDATE `users` SET `' . $record . '` = ? WHERE `id` = ?');
        $query->bind_param('ss', $data, $id);
        if ($query->execute()) {
            if ($query->affected_rows > 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function getTotalUsersNumber()
    {
        $query = $this->con->mysqli->query('SELECT * FROM `users`');
        $num = $query->num_rows;
        return $num;
    }

    public function getUsersNumber($param, $data)
    {
        $query = $this->con->mysqli->query('SELECT * FROM `users` WHERE `' . $param . '` = "' . $data . '"');
        $num = $query->num_rows;
        return $num;
    }

    public function getLatestRegistrant()
    {
        $query = $this->con->mysqli->query('SELECT * FROM `users` ORDER BY `id` DESC LIMIT 1');
        $array = $query->fetch_assoc();  
        return $array;
    }
}