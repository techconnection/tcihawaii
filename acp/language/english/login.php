<?php

/**
 * Title
 */
$_['title'] = 'Login';

/**
 * Description
 */
$_['description'] = 'This is the login page description and it is about 160 characters long, which is super important for seo or (search engine optimization). Try to keep it so.';

/**
 * Alerts
 */
$_['login_fail'] = '<div class="alert error"><strong>Error!</strong> The email and/or password you entered was incorrect. <button type="button" class="alert-close"><i class="fas fa-times fa-fw"></i></button></div>';
$_['locked_5'] = '<div class="alert error"><strong>Error!</strong> This account has been locked for 5 minutes becuase of too many failed login attempts. <button type="button" class="alert-close"><i class="fas fa-times fa-fw"></i></button></div>';
$_['locked'] = '<div class="alert error"><strong>Error!</strong> This account has been locked. <button type="button" class="alert-close"><i class="fas fa-times fa-fw"></i></button></div>';