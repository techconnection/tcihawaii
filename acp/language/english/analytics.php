<?php 

/**
 * Title
 */
$_['title'] = 'Analytics';

/**
 * Alerts
 */
$_['code_updated'] = '<div class="alert success"><strong>Success!</strong> Your analytics code has been saved. (<a href="">Continue?</a>) <button type="button" class="alert-close"><i class="fas fa-times fa-fw"></i></button></div>';
$_['code_not_updated'] = '<div class="alert error"><strong>Error!</strong> Your analytics code has not been saved. (<a href="">Try Again?</a>) <button type="button" class="alert-close"><i class="fas fa-times fa-fw"></i></button></div>';