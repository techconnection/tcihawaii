<?php 

/**
 * Title
 */
$_['title'] = 'Newsletter';

/**
 * Alerts
 */
$_['newsletter_success'] = '<div class="alert success"> The newsletter has been sent to the subscribers. <a href="">Continue</a> <button type="button" class="alert-close"><i class="fas fa-times fa-fw"></i></button></div>';
$_['newsletter_fail'] = '<div class="alert error"> The message could not be sent. <a href="">Try Again</a> <button type="button" class="alert-close"><i class="fas fa-times fa-fw"></i></button></div>';