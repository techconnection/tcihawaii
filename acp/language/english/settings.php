<?php 

/**
 * Title
 */
$_['title'] = 'Settings';

/**
 * Alerts
 */
$_['setting_updated'] = '<div class="alert success"><strong>Success!</strong> Settings has been updated. <button type="button" class="alert-close"><i class="fas fa-times fa-fw"></i></button></div>';
$_['log_cleared'] = '<div class="alert success"><strong>Success!</strong> The log has been cleared. <button type="button" class="alert-close"><i class="fas fa-times fa-fw"></i></button></div>';