<?php 

/**
 * Title
 */
$_['title'] = 'Pages';

/**
 * Alerts
 */
$_['page_created'] = '<div class="alert success"><strong>Success!</strong> The page has been created. <button type="button" class="alert-close"><i class="fas fa-times fa-fw"></i></button></div>';
$_['page_updated'] = '<div class="alert success"><strong>Success!</strong> Your content has been saved. <button type="button" class="alert-close"><i class="fas fa-times fa-fw"></i></button></div>';
$_['page_deleted'] = '<div class="alert success"><strong>Success!</strong> The page has been deleted. <button type="button" class="alert-close"><i class="fas fa-times fa-fw"></i></button></div>';
$_['no_name'] = '<div class="alert warning"><strong>Warning!</strong> Please enter a title to create a new page. <button type="button" class="alert-close"><i class="fas fa-times fa-fw"></i></button></div>';
$_['file_invalid'] = '<div class="alert error"><strong>Error!</strong> File denied. Excepted files types for avatars are: jpg | jpeg | png | tif | tiff | gif. <a href="">Try Again</a> <button type="button" class="alert-close"><i class="fas fa-times fa-fw"></i></button></div>';
$_['file_large'] = '<div class="alert error"><strong>Error!</strong> File denied. The size limit for avatars is 1MB. <button type="button" class="alert-close"><i class="fas fa-times fa-fw"></i></button></div>';
$_['file_uploaded'] = '<div class="alert success"><strong>Success!</strong> The image for this blog post has been uploaded. <button type="button" class="alert-close"><i class="fas fa-times fa-fw"></i></button></div>';
$_['file_upload_fail'] = '<div class="alert error"><strong>Error!</strong> The image for this blog post cannot be uploaded. <button type="button" class="alert-close"><i class="fas fa-times fa-fw"></i></button></div>';