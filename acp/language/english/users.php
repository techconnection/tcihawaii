<?php 

/**
 * Title
 */
$_['title'] = 'Users';

/**
 * Alerts
 */
$_['no_user'] = '<div class="alert">Select a user</div>';
$_['user_updated'] = '<div class="alert success"><strong>Success!</strong> The user was set to ({{group}}). <button type="button" class="alert-close"><i class="fas fa-times fa-fw"></i></button></div>';