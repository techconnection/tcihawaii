<?php 

/**
 * Title
 */
$_['title'] = 'Blog';

/**
 * Alerts
 */
$_['blog_title'] = 'Default Title.';
$_['post_saved'] = '<div class="alert success"><strong>Success!</strong> Your post has been added to the blog. <button type="button" class="alert-continue"><i class="fas fa-times fa-fw"></i></button></div>';
$_['post_updated'] = '<div class="alert success"><strong>Success!</strong> Your blog post has been updated. <button type="button" class="alert-continue"><i class="fas fa-times fa-fw"></i></button></div>';
$_['post_deleted'] = '<div class="alert success"><strong>Success!</strong> The post has been deleted from the blog. (<a href="">Continue</a>) <button type="button" class="alert-close"><i class="fas fa-times fa-fw"></i></button></div>';
$_['file_invalid'] = '<div class="alert error"><strong>Error!</strong> File denied. Excepted files types for avatars are: jpg | jpeg | png | tif | tiff | gif. (<a href="">Try Again</a>) <button type="button" class="alert-close"><i class="fas fa-times fa-fw"></i></button></div>';
$_['file_large'] = '<div class="alert error"><strong>Error!</strong> File denied. The size limit for avatars is 1MB. (<a href="">Try Again</a>) <button type="button" class="alert-close"><i class="fas fa-times fa-fw"></i></button></div>';
$_['file_uploaded'] = '<div class="alert success"><strong>Success!</strong> The image for this blog post has been uploaded. <button type="button" class="alert-close"><i class="fas fa-times fa-fw"></i></button></div>';
$_['file_upload_fail'] = '<div class="alert error"><strong>Error!</strong> The image for this blog post cannot be uploaded. <button type="button" class="alert-close"><i class="fas fa-times fa-fw"></i></button></div>';