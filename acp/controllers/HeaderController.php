<?php 

/**
 * Header Controller Class
 *
 * The HeaderController handles logic specific to the header and displays the 
 * header view. The HeaderController should be loaded in each controller class 
 * where a header is desired.
 */
class HeaderController extends Controller
{
    /**
     * Init method
     *
     * The init methods in controller classes will be called automatically when a 
     * controller is loaded. 
     *
     * Assuming that the HeaderController should be loaded in every controller 
     * this init method should run in every controller too.
     */
    public function init()
    {     
        if ($this->session->isLogged()) {
            $user = $this->load->model('user')->getUser('id', $this->session->id);
        }

        $data['logged'] = $user; 

        switch ($this->url[0]) {
            case '':
                $this->load->route('/acp/overview');
                break;
            default:
                $data['title'] = $this->language->get($this->url[0] . '/title');
                break;
        }

        return $this->load->view('header', $data);
    }
}