<?php 

/**
 * Blog Controller Class
 *
 * This class gets blog data from the database and sets variables for 
 * display in the view.
 */
class BlogController extends Controller
{
    public function init($view = null, $orderby = null, $direction = null, $page = null, $limit = null, $page_total = null)
    {
        $blog_model = $this->load->model('blog');
        $orderby = $this->load->model('blog')->checkOrderby($orderby);
        $paginated = $this->helper->paginate($page, $limit, $blog_model, 'blog', $orderby, $direction);
        $total_pages = $paginated['pages'] == 0 ? 'na' : $paginated['pages'];

        if ($view) {
            if ($view === 'list') {
                if (!$page || !$limit) {
                    $this->load->route('/acp/blog/list/id/asc/' . 1 . '/' . 15);
                }
                if (!$page_total) {
                    $this->load->route('/acp/blog/list/' . $orderby . '/' . $direction . '/' . $page . '/' . $limit . '/' . $total_pages);
                }
                $this->drawBlogList($paginated);
            }
            if ($view === 'new') {
                $this->edit();
            }
        } else {
            $this->load->route('/acp/blog/list');
        }
    }

    public function drawBlogList($paginated) 
    {
        $data['header'] = $this->load->controller('header');
        $data['footer'] = $this->load->controller('footer');
        $data['search'] = $this->load->view('search');
        $data['nav'] = $this->load->view('nav');
        $data['breadcrumb'] = $this->load->controller('breadcrumb');

        $posts = $paginated['list'];
        if ($posts) {
            foreach ($posts as $post) {
                $views = $post['views'] ? $post['views'] : 0;
                $last_view = $post['last_view'] ? date('d M, Y (g:ia)', strtotime($post['last_view'])) : 'Never';
                $last_edit = $post['last_edit'] ? date('d M, Y (g:ia)', strtotime($post['last_edit'])) : 'Never';
                $data['blog_posts'][] = [
                    'id' => $post['id'],
                    'author' => $post['author'],
                    'title' => $post['title'],
                    'body' => $post['body'],
                    'views' => $views,
                    'last_view' => $last_view,
                    'last_edit' => $last_edit,
                    'posted' => date('d M, Y (g:ia)', strtotime($post['date_posted']))
                ];
            }
        } else {
            $data['blog_posts'] = [];
        }

        $data['list'] = $this->load->view('list-blogs', $data);
        $data['controls'] = 'blog_controls';
        $data['controls'] = $this->load->view('list-controls', $data);

        exit($this->load->view('list', $data));
    }

    public function insert()
    {   
        if (isset($_POST['body'])) {
            $this->session->isLogged();
            
            $user = $this->load->model('user')->getUser('id', $this->session->id);
            $blog_model = $this->load->model('blog');
            $author = $user['username'];
            $title = 'Blog Title';
            $body = trim($_POST['body']);
            $preview_image = null;

            if (preg_match('/<h1[^>]+>(.*?)<\/h1>/', $body, $title_matches) == true || preg_match('/<h1>(.*?)<\/h1>/', $body, $title_matches) == true) {
                $title = $title_matches[1]; 
            }

            if (preg_match('/(<img[^>]+>)/i', $body, $image_matches) == true) {
                if (isset($image_matches[0])) {
                    $preview_image = $image_matches[0];
                }
            }

            if ($blog_model->insertPost($author, $title, $body, $preview_image)) {
                $this->log->insertLog('Administrator (' . $author. ') posted a new blog (' . $title . ').');
                exit($this->language->get('blog/post_saved'));
            } else {
                exit('Post not savead');
            }
        }
    }

    public function edit($id = null)
    {
        $data['header'] = $this->load->controller('header');
        $data['footer'] = $this->load->controller('footer');
        $data['search'] = $this->load->view('search');
        $data['nav'] = $this->load->view('nav');
        $data['breadcrumb'] = $this->load->controller('breadcrumb');

        if ($id) {
            $post = $this->load->model('blog')->getPost('id', $id);

            $data['id'] = $id;
            $data['title'] = $post['title'];
            $data['body'] = $post['body'];
            
            exit($this->load->view('edit-blog', $data)); 
        } else {
            exit($this->load->view('new-blog', $data)); 
        }
    }

    public function update()
    {
        $id = $_POST['id'];
        $title = 'Blog Title';
        $body = trim($_POST['body']);
        $preview_image = null;
        $user = $this->load->model('user')->getUser('id', $this->session->id);

        if (preg_match('/<h1[^>]+>(.*?)<\/h1>/', $body, $title_matches) == true || preg_match('/<h1>(.*?)<\/h1>/', $body, $title_matches) == true) {
            $title = $title_matches[1]; 
        }

        if (preg_match('/(<img[^>]+>)/i', $body, $image_matches) == true) {
            if (isset($image_matches[0])) {
                $preview_image = $image_matches[0];
            }
        }

        if ($this->load->model('blog')->updateBlog($title, $body, $preview_image, $id)) {
            $this->log->insertLog('Administrator (' . $user['username'] . ') edited blog post (' . $title . ').');
            exit($this->language->get('blog/post_updated'));
        } else {
            exit('Post not updated.');
        }
    }

    public function delete($id)
    {
        $blog_model = $this->load->model('blog');
        $blog_model->deletePost($id);
        if ($blog_model->deletePost($id)) {
            $this->log->insertLog('Administrator (' . $this->session->username . ') deleted blog number (' . $id . ').');
            $this->load->route('/acp/blog');
        }
    }

    public function upload()
    {
        $user = $this->load->model('user')->getUser('id', $this->session->id);
        $upload_dir = $_POST['upload_dir'];
        $upload_library = $this->load->library('Upload');

        $upload_library->uploadImage($_FILES['upload_image'], $upload_dir);

        if ($upload_library->file_invalid) {
            exit($this->language->get('account/file_invalid'));
        }
        if ($upload_library->filebig) {
            exit($this->language->get('account/file_big'));
        }

        if ($upload_library->upload_success) {
            $this->log->insertLog('Administrator (' . $user['username'] . ') uploaded a file (' . $upload_library->filename . ') to (' . $upload_dir . ').');
            exit($this->language->get('pages/file_uploaded')); 
        }
    }
}