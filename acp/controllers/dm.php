<?php 

/**
 * Direct Message Controller Class
 *
 * 
 */
class DmController extends Controller
{
    public function init()
    {
        $data['header'] = $this->load->controller('header');
        $data['footer'] = $this->load->controller('footer');
        $data['breadcrumb'] = $this->load->controller('breadcrumb');
        $data['search'] = $this->load->view('search');

        exit($this->load->view('users/list', $data));
    }
}