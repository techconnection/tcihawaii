<?php 

/**
 * Home Controller Class
 *
 *
 *
 *
 */
class OverviewController extends Controller
{
    /**
    * Access the newsletter model.
    * @var object
    */
    private $subscriber;

    public function init()
    {   
        $this->user_model = $this->load->model('user');
        $this->pages_model = $this->load->model('pages');
        $this->newsletter_model = $this->load->model('newsletter');
        $this->blog_model = $this->load->model('blog');

        $this->getUserInfo();
        $this->getPagesInfo();
        $this->getSubscriberInfo();
        $this->getBlogInfo();

        $data['total_users'] = $this->total_users;
        $data['total_registered'] = $this->total_registered;
        $data['total_moderators'] = $this->total_mods;
        $data['total_administrators'] = $this->total_admins;
        $data['total_banned'] = $this->total_banned;
        $data['latest_registrant'] = $this->latest_registrant['username'];
        $data['total_subscriptions'] = $this->total_subscriptions;
        $data['registered_subscribers'] = $this->registered_subscribers;
        $data['unregistered_subscribers'] = $this->unregistered_subscribers;
        $data['latest_subscription'] = $this->latest_subscription;
        $data['total_posts'] = $this->total_posts;
        $data['blog_views'] = $this->blog_views;
        $data['most_viewed_blog_title'] = $this->most_viewed_blog_title;
        $data['most_viewed_blog_route'] = $this->most_viewed_blog_route;
        $data['total_pages'] = $this->total_pages;
        $data['last_post'] = $this->last_post;
        $data['last_post_title'] = $this->last_post_title;
        $data['last_post_id'] = $this->last_post_id;
        $data['last_edit_time'] = $this->last_edit_time;
        $data['last_edit_title'] = $this->last_edit_title;
        $data['last_edit_id'] = $this->last_edit_id;
        $data['total_pages'] = $this->total_pages;
        $data['most_viewed_page_title'] = $this->most_viewed_page_title;
        $data['most_viewed_page_route'] = $this->most_viewed_page_route;
        $data['most_viewed_page_views'] = $this->most_viewed_page_views;
        $data['total_page_views'] = $this->total_page_views;
        
        $users = $this->user_model->getAll('users');
        foreach ($users as $user) {
            if ($this->checkOnline($user['username']) === true) {
                $is_online = '<span class="text-green">Online</span>';
            } else {
                $is_online = '<span class="text-red">Offline</span>';
            }
            switch ($user['group']) {
                case '1':
                    $group = 'Registered';
                    break;
                case '2':
                    $group = 'Moderator';
                    break;
                case '3':
                    $group = 'Admin';
                    break;
                default:
                    $group = 'Unregistered';
                    break;
            }
            $data['users'][] = [
                'id' => $user['id'],
                'firstname' => $user['firstname'],
                'lastname' => $user['lastname'],
                'username' => $user['username'],
                'email' => $user['email'],
                'signup_date' => date('d M, Y', strtotime($user['signup_date'])),
                'last_active' => $is_online,
                'group' => $group
            ];
        }

        $data['logs'] = [];
        $logs = $this->log->getLog();

        if (!empty($logs)) {
            foreach ($logs as $log) {
                $data['logs'][] = [
                    'id' => $log['id'],
                    'time' => date('d/m/Y h:ia', strtotime($log['time'])),
                    'event' => $log['event']
                ];
            }
        } 

        $data['header'] = $this->load->controller('header');
        $data['footer'] = $this->load->controller('footer');
        $data['search'] = $this->load->view('search');
        $data['nav'] = $this->load->view('nav');
        $data['breadcrumb'] = $this->load->controller('breadcrumb');

        exit($this->load->view('home', $data));
    }

    protected function checkOnline($username) 
    {
        $user = $this->user_model->getUser('username', $username);
        $last_active = (int) ($user['last_active']);
        if (time() - $last_active > 1 * 60) {
            return false;
        } else {
            return true;
        }
    }

    protected function getUserInfo()
    {
        $this->total_users = $this->user_model->getTotalUsersNumber();
        $this->total_registered = $this->user_model->getUsersNumber('group', 1);
        $this->total_mods = $this->user_model->getUsersNumber('group', 2);
        $this->total_admins = $this->user_model->getUsersNumber('group', 3);
        $this->total_banned = $this->user_model->getUsersNumber('group', 4);
        $this->latest_registrant = $this->user_model->getLatestRegistrant();
    }

    protected function getPagesInfo()
    {
        $this->total_pages = $this->pages_model->getTotalPageNumber();
        $most_viewed = $this->pages_model->getMostViewed();
        $this->most_viewed_page_title = $most_viewed['title'];
        $this->most_viewed_page_route = $most_viewed['route'];
        $this->most_viewed_page_views = $most_viewed['views'];
        $this->total_page_views = $this->pages_model->getTotalPageViews();
    }

    protected function getSubscriberInfo()
    {
        $all_users = $this->user_model->getAll('users');
        $all_subscriptions = $this->newsletter_model->getSubscribers('status', 1);
        $users = [];
        $subscriptions = [];

        foreach ($all_users as $user) {
            $users[] = $user['email'];
        }

        foreach ($all_subscriptions as $subscription) {
            $subscriptions[] = $subscription['email'];
        }

        $this->total_subscriptions = $this->newsletter_model->getTotalSubscriptionsNumber();
        $this->registered_subscribers = count(array_intersect($users, $subscriptions));

        if ($this->total_subscriptions > 0 && $this->registered_subscribers > 0) {
            $this->unregistered_subscribers = $this->registered_subscribers % $this->total_subscriptions;
        } else {
            $this->unregistered_subscribers = $this->total_subscriptions;
        }

        $last_subscription = $this->newsletter_model->getLastSubscription();
        $this->latest_subscription = isset($last_subscription) ? date('D, j M, Y', strtotime($last_subscription['date_subscribed'])) : 'n\\a' ;
    }

    protected function getBlogInfo() 
    {
        $this->total_posts = $this->blog_model->getTotalPostsNumber();
        $this->blog_views = $this->blog_model->getViewsNumber();
        $most_viewed = $this->blog_model->getMostViewed();
        $this->most_viewed_blog_title = $most_viewed['title'];
        $most_viewed_title = $most_viewed['title'];
        $most_viewed_title = str_replace(' ', '_', $most_viewed['title']);
        $most_viewed_id = $most_viewed['id'];
        $this->most_viewed_blog_route = '/blog/' . $most_viewed_id . '/' . strtolower($most_viewed_title); 
        $last_post = $this->blog_model->getLastPost();
        $this->last_post = isset($last_post) ? date('D, j M, Y', strtotime($last_post['date_posted'])) : 'n\\a' ;
        $this->last_post_title = $last_post['title'];
        $this->last_post_id = $last_post['id'];
        $last_edit = $this->blog_model->getLastEdited();
        $this->last_edit_time = isset($last_edit) ? date('D, j M, Y', strtotime($last_edit['last_edit'])) : 'n\\a' ;
        $this->last_edit_title = $last_edit['title'];
        $this->last_edit_id = $last_edit['id'];
    }

    public function clearLog()
    {
        if ($this->log->clearLog()) {
            exit($this->language->get('settings/log_cleared'));
        }
    }
}