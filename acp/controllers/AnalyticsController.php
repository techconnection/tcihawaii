<?php 

/**
 * Analytics Controller Class
 */
class AnalyticsController extends Controller
{
    public function init()
    {
        $analytics_model = $this->load->model('analytics');
        $analytics_code = $analytics_model->getAnalyticsCode();
        
        $data['analytics_code'] = $analytics_code['code'];
        $data['header'] = $this->load->controller('header');
        $data['footer'] = $this->load->controller('footer');
        $data['search'] = $this->load->view('search');
        $data['nav'] = $this->load->view('nav');
        $data['breadcrumb'] = $this->load->controller('breadcrumb');

        exit($this->load->view('analytics', $data));
    }

    public function validate()
    {
        $code = '<script type="text/javascript">' . $_POST['code'] . '</script>';
        
        if ($this->load->model('analytics')->updateAnalyticsCode($code)) {
            exit($this->language->alert('code_updated'));
        } else {
            exit($this->language->alert('code_not_updated'));
        }
    }
}