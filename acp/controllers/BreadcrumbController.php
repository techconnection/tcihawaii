<?php 

/**
 * Breadcrumb Controller Class
 *
 * This class gets blog data from the database and sets variables for 
 * display in the view.
 */
class BreadcrumbController extends Controller
{
    public function init()
    {
        $data['breadcrumbs'] = [];

        $links = isset($_GET['url']) ? $this->helper->splitUrl($_GET['url']) : null;

        if ($links) {
            $paths = [];
            
            while (!empty($links)) {
                array_push($paths, implode('/', $links));
                array_pop($links);
            }

            sort($paths);
            foreach ($paths as $path) {
                $path_array = explode('/', $path);

                $data['breadcrumbs'][] = [
                    'link' => $path,
                    'crumb' => str_replace('_', ' ', strtoupper(end( $path_array)))
                ];
            }
        }

        return $this->load->view('breadcrumb', $data);
    }
}