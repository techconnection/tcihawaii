<?php 

/**
 * Search Controller Class
 *
 * This SearchController class interacts with the SearchModel to find results 
 * in the database matching the search string and pushes them to the view.
 */
class SearchController extends Controller
{
    /**
     * Init method
     *
     * The init methods in controller classes will be called automatically when a 
     * controller is loaded. 
     *
     * Routes
     * - http://root/search
     * - http://root/search/init
     * 
     * @param string $string      
     */
    public function init()
    {   
        $users = [];
        $posts = [];
        $pages = [];

        if (!empty($_POST['string'])) {
            $string = $_POST['string'];

            // Load the search model.
            $search_model = $this->load->model('search');

            // Search these tables.
            $users = $search_model->searchUsers($string);
            $posts = $search_model->searchBlogPosts($string);
            $pages = $search_model->searchPages($string);
        }

        $json = ['users' => $users, 'posts' => $posts, 'pages' => $pages];

        echo json_encode($json);
    }
}