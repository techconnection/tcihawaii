<?php 

/**
 * Settings Controller Class
 */
class SettingsController extends Controller
{
    public function init()
    {
        $settings_model = $this->load->model('settings');

        $data['header'] = $this->load->controller('header');
        $data['footer'] = $this->load->controller('footer');
        $data['search'] = $this->load->view('search');
        $data['nav'] = $this->load->view('nav');
        $data['breadcrumb'] = $this->load->controller('breadcrumb');
        $data['owners_email'] = $settings_model->getSetting('owners_email');
        $data['pw_setting'] = $settings_model->getSetting('strong_pw');
        $data['logout_setting'] = $settings_model->getSetting('verify_logout');
        $data['inactivity_setting'] = $settings_model->getSetting('inactivity_limit');
        $data['sitename_setting'] = $settings_model->getSetting('sitename');
        $data['mode_setting'] = $settings_model->getSetting('maintenance_mode');
        $data['mail'] = $settings_model->getMailSettings();

        $timezones = [];
        foreach (timezone_abbreviations_list() as $timezone) {
            foreach ($timezone as $tz) {
                array_push($timezones, $tz['timezone_id']);
            }
        }

        sort($timezones);
        $sorted_timezones = [];
        foreach ($timezones as $tz) {
            if ($tz !== NULL) {
                array_push($sorted_timezones, $tz);
            }
        }

        $data['timezones'] = array_unique($sorted_timezones);

        exit($this->load->view('settings', $data));
    }

    public function update()
    {
        $this->settings_model = $this->load->model('settings');

        $this->updateOwnersEmailSetting();
        $this->updatePasswordSetting();
        $this->updateLogoutSetting();
        $this->updateInactiveSetting();
        $this->updateSitenameSetting();
        $this->updateMaintenanceSetting();
        $this->updateMailSettings();
        
        exit($this->language->get('settings/setting_updated'));
    }

    public function updateOwnersEmailSetting()
    {
        $owners_email = $_POST['owners_email'];

        if (!$this->settings_model->updateSetting($owners_email, 6)) {
            exit('Could not update owners email setting');
        }
    }

    public function updateSitenameSetting()
    {
        $sitename = $_POST['sitename'];

        if (!$this->settings_model->updateSetting($sitename, 1)) {
            exit('Could not update sitename setting');
        }
    }

    public function updatePasswordSetting()
    {
        if (isset($_POST['pw_setting'])) {
            $strong_pw = 1;
        } else {
            $strong_pw = null;
        }
        if (!$this->settings_model->updateSetting($strong_pw, 12)) {
            exit('Could not update password setting');
        }
    }

    public function updateLogoutSetting()
    {
        if (isset($_POST['logout_setting'])) {
            $verify_logout = 1;
        } else {
            $verify_logout = null;
        }

        if (!$this->settings_model->updateSetting($verify_logout, 3)) {
            exit('Could not update logout setting');
        }
    }

    public function updateInactiveSetting()
    {
        $inactivity_limit = $_POST['inactivity_setting'];

        if (!$this->settings_model->updateSetting($inactivity_limit, 4)) {
            exit('Could not update inactivity setting');
        }
    }

    public function updateMaintenanceSetting()
    {
        if (isset($_POST['mode_setting'])) {
            $maintenance_mode = 1;
        } else {
            $maintenance_mode = null;
        }

        if (!$this->settings_model->updateSetting($maintenance_mode, 5)) {
            exit('Could not update maintenance mode setting');
        }
    }

    public function updateMailSettings()
    {
        $mail_host = $_POST['mail_host'];
        $mail_port = $_POST['mail_port'];
        $mail_username = $_POST['mail_username'];
        $mail_password = $_POST['mail_password'];
        $mail_setttings = [
            'host' => $mail_host, 
            'port' => $mail_port, 
            'username' => $mail_username, 
            'password' => $mail_password
        ];
        foreach ($mail_setttings as $key => $value) {
            if (!$this->settings_model->updateMailSettings($key, $value)) {
                exit('Could not update mail settings');
            }
        }
    }

    public function getThemeSetting()
    {
        if ($this->session->isLogged()) {
            $user_model = $this->load->model('user');
            $user = $user_model->getUser('id', $this->session->id);

            echo $user['theme'];
        }
    }

    public function changeThemeSetting() 
    {
        if ($this->session->isLogged()) {

            $user_model = $this->load->model('user');
            $user = $user_model->getUser('id', $this->session->id);
            $theme = $user['theme'];

            if ($theme == 0) {
                $theme = 1;
                $user_model->updateUser('theme', $theme, $this->logged_user['id']);
            } elseif ($theme == 1) {
                $theme = 0;
                $user_model->updateUser('theme', $theme, $this->logged_user['id']);
            }

            echo $theme;
        }
    }

    public function getMenuSetting()
    {
        if ($this->session->isLogged()) {
            $user_model = $this->load->model('user');
            $user = $user_model->getUser('id', $this->session->id);

            echo $user['menu'];
        }
    }

    public function changeMenuSetting() 
    {
        if ($this->session->isLogged()) {

            $user_model = $this->load->model('user');
            $user = $user_model->getUser('id', $this->session->id);
            $menu = $user['menu'];

            if ($menu == 0) {
                $menu = 1;
                $user_model->updateUser('menu', $menu, $this->logged_user['id']);
            } elseif ($menu == 1) {
                $menu = 0;
                $user_model->updateUser('menu', $menu, $this->logged_user['id']);
            }

            echo $menu;
        }
    }
}