<?php 

/**
 * Sitemap Controller Class
 *
 * The sitemap controller is responsible for finding all the links in the host. 
 * It also creates an xml version of the sitemap and prepares the data to be 
 * displayed in the view and saved to the database.
 */
class SitemapController extends Controller
{
    /**
     * Init method
     *
     * The init methods in controller classes will be called automatically when a 
     * controller is loaded. 
     *
     * Routes
     * - http://root/sitemap
     * - http://root/sitemap/init
     * - http://root/information/sitemap
     *
     * This method will access other methods in the class to do a few tasks 
     * related to the sitemap and display the sitemap view.
     */
    public function init($view = null, $orderby = null, $direction = null, $page = null, $limit = null, $page_total = null)
    {
        $pages_model = $this->load->model('pages');
        $orderby = $this->load->model('pages')->checkOrderby($orderby);
        $paginated = $this->helper->paginate($page, $limit, $pages_model, 'sitemap', $orderby, $direction);
        $total_pages = $paginated['pages'] == 0 ? 'na' : $paginated['pages'];

        if ($view) {
            if ($view === 'list') {
                if (!$page || !$limit) {
                    $this->load->route('/acp/sitemap/list/id/asc/' . 1 . '/' . 15);
                }
                if (!$page_total) {
                    $this->load->route('/acp/sitemap/list/' . $orderby . '/' . $direction . '/' . $page . '/' . $limit . '/' . $total_pages);
                }
                $this->drawSitemapList($paginated);
            }
        } else {
            $this->load->route('/acp/sitemap/list');
        }
    }

    public function drawSitemapList($paginated) 
    {
        $data['header'] = $this->load->controller('header');
        $data['footer'] = $this->load->controller('footer');
        $data['search'] = $this->load->view('search');
        $data['nav'] = $this->load->view('nav');
        $data['breadcrumb'] = $this->load->controller('breadcrumb');
        $data['sitemap_xml'] = $this->checkSitemapXMLExists();
        $data['sitemap_records'] = $this->checkSitemapRecordsExist();
        $data['pages']= [];

        $pages = $paginated['list'];
        foreach ($pages as $p) {
            if ($p['hidden'] == 0) {
                $hidden = 'False';
            } elseif ($p['hidden'] == 1) {
                $hidden = 'True';
            }
            $data['pages'][] = [
                'id' => $p['id'],
                'name' => $p['name'],
                'route_name' => $p['route_name'],
                'route' => $p['route'],
                'link_order' => $p['link_order'],
                'hidden' => $hidden,
            ];
        }
        $data['list'] = $this->load->view('list-sitemap', $data);
        $data['controls'] = 'sitemap_controls';
        $data['controls'] = $this->load->view('list-controls', $data);

        exit($this->load->view('list', $data));
    }

    public function generate()
    {
        $this->insertSitemapDatabaseRecords();
        $this->generateSitemapXML();
        $this->load->route('/acp/sitemap/list');
    }

    public function erase()
    {
        $this->load->model('pages')->eraseSitemap();
        $this->load->route('/acp/sitemap/list');
    }

    public function update($record, $value, $name)
    {
        $sitelink = $this->load->model('pages')->getSiteLink($name);
        if ($sitelink) {
            if ($value == 'true') {
                $value = 0;
            } elseif ($value == 'false') {
                $value = 1;
            }
            $this->load->model('pages')->updateSitemap($record, $value, $name);
            $this->load->route('/acp/sitemap/list');
        }
    }

    public function save()
    {
        $post = $_POST;
        foreach ($post as $p => $post) {
            if ($this->load->model('pages')->updateSitemap('link_order', $post, $p)) {
                echo 'Saved!';
            }
        }
    }

    public function insertSitemapDatabaseRecords() 
    {
        $pages_model = $this->load->model('pages');
        $pages = $pages_model->getAll('pages');
        $sitelinks = $pages_model->getSiteLinks();
        $signup_page = ['name' => 'signup', 'route_name' => 'Signup', 'route' => '/signup'];
        $login_page = ['name' => 'login', 'route_name' => 'Login', 'route' => '/login'];
        $account_page = ['name' => 'account', 'route_name' => 'Account', 'route' => '/account'];
        $profile_page = ['name' => 'profile', 'route_name' => 'Profile', 'route' => '/profile'];
        $num = 1;

        array_push($pages, $signup_page);
        array_push($pages, $login_page);
        array_push($pages, $account_page);
        array_push($pages, $profile_page);

        foreach ($pages as $p) {
            if (!$pages_model->getSiteLink($p['name'])) {
                $pages_model->insertSiteLink($p['name'], $this->generateRouteName($p['name']), $p['route'], $num++);
            }
        }
    }

    public function generateRouteName($name) 
    {
        if (strpos($name, '-')) {
            $name_array = explode('-', $name);
            $route_name = [];
            foreach ($name_array as $na) {
                array_push($route_name, ucfirst($na));
            }
            return implode(' ', $route_name);
        } else {
            return ucfirst($name);
        }
    }

    /**
     * Create the xml sitemap
     * 
     * Using the links found in the findLinks method generate or alter a
     * sitemap.xml file and write the sitemap data to the file.
     */
    private function generateSitemapXML()
    {
        // Open sitemap.xml or create the file if it doesn't exist. If php cannot open or 
        // create the file then exit.
        $sitemap = fopen(APPLICATION_DIR . '/views/sitemap.xml', 'w') or exit('Unable to open file!');
        $sitelinks = $this->load->model('pages')->getSiteLinks();

        // Write the beggining of the sitemap.xml.
        fwrite($sitemap, "<?xml version=\"1.0\" encoding=\"UTF-8\"?> \n");
        fwrite($sitemap, "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\"> \n");

        // Write the links to the sitemap.xml. The spaces in the fwrite are necessary
        // for the xml to be formatted neatly.
        foreach ($sitelinks as $sl) {
            fwrite($sitemap, "    <url>\n        <loc>" . HOST . $sl['route'] . "</loc>\n        <changefreq>weekly</changefreq>\n    </url> \n");
        }

        // Write the ending of the sitemap.xml.
        fwrite($sitemap, '</urlset>');
    }

    public function checkSitemapXMLExists()
    {
        if (file_exists(APPLICATION_DIR . '/views/sitemap.xml')) {
            return true;
        } else {
            return false;
        }
    }

    public function checkSitemapRecordsExist()
    {
        if ($this->load->model('pages')->getSiteLinks()) {
            return true;
        } else {
            return false;
        }
    }
}