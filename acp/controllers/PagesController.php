<?php 

/**
 * Pages Controller Class
 *
 *
 * 
 */
class PagesController extends Controller
{
    public function init($view = null, $orderby = null, $direction = null, $page = null, $limit = null, $page_total = null)
    {
        $pages_model = $this->load->model('pages');
        $orderby = $this->load->model('pages')->checkOrderby($orderby);
        $paginated = $this->helper->paginate($page, $limit, $pages_model, 'pages', $orderby, $direction);
        $total_pages = $paginated['pages'] == 0 ? 'na' : $paginated['pages'];

        if ($view) {
            if ($view === 'list') {
                if (!$page || !$limit) {
                    $this->load->route('/acp/pages/list/id/asc/' . 1 . '/' . 15);
                }
                if (!$page_total) {
                    $this->load->route('/acp/pages/list/' . $orderby . '/' . $direction . '/' . $page . '/' . $limit . '/' . $total_pages);
                }
                $this->drawPageList($paginated);
            }
            if ($view === 'new') {
                $this->drawPageMaker();
            }
        } else {
            $this->load->route('/acp/pages/list');
        }
    }

    public function drawPageList($paginated) 
    {
        $data['header'] = $this->load->controller('header');
        $data['footer'] = $this->load->controller('footer');
        $data['search'] = $this->load->view('search');
        $data['nav'] = $this->load->view('nav');
        $data['breadcrumb'] = $this->load->controller('breadcrumb');
        $data['url'] = HOST;

        $pages = $paginated['list'];
        foreach ($pages as $page) {
            $views = $page['views'] ? $page['views'] : 0;
            $last_view = $page['last_view'] ? date('d M, Y (g:ia)', strtotime($page['last_view'])) : 'Never';
            $locked = $page['locked'] == 0 ? null : true;
            $data['pages_list'][] = [
                'id' => $page['id'],
                'name' => $page['name'],
                'title' => $page['title'],
                'route' => $page['route'],
                'content' => $page['content'],
                'views' => $views,
                'last_view' => $last_view,
                'last_edit' => date('d M, Y (g:ia)', strtotime($page['last_edit'])),
                'link' => $page['name'],
                'locked' => $locked
            ];
        }

        $data['list'] = $this->load->view('list-pages', $data);
        $data['controls'] = 'page_controls';
        $data['controls'] = $this->load->view('list-controls', $data);

        exit($this->load->view('list', $data));
    }

    public function edit($page_name = null)
    {
        $data['header'] = $this->load->controller('header');
        $data['footer'] = $this->load->controller('footer');
        $data['search'] = $this->load->view('search');
        $data['nav'] = $this->load->view('nav');
        $data['breadcrumb'] = $this->load->controller('breadcrumb');

        if (!$page_name) {
            $this->load->route('/acp/pages/');
        }

        $page = $this->load->model('pages')->getPage($page_name);

        $data['content'] = $page['content'];
        $data['title'] = $page['title'];
        $data['description'] = $page['description'];
        $data['name'] = $page['name'];

        exit($this->load->view('edit-pages', $data));
    }

    public function update()
    {
        $title = $_POST['title'];
        $description = $_POST['description'];
        $content = $_POST['custom_content'];
        $name = $_POST['name'];
        $pages_model = $this->load->model('pages');
        $page = $pages_model->getPage($name);
        $user = $this->load->model('user')->getUser('id', $this->session->id);

        $pages_model->updatePage('title', $title, $name);
        $pages_model->updatePage('description', $description, $name);
        $pages_model->updatePage('content', $content, $name);
        $this->log->insertLog('Administrator (' . $user['username'] . ') edited the (' . $page['name'] . ') page.');

        exit($this->language->get('pages/page_updated'));
    }

    public function upload()
    {
        $user = $this->load->model('user')->getUser('id', $this->session->id);
        $upload_dir = $_POST['upload_dir'];
        $upload_library = $this->load->library('Upload');
        
        $upload_library->uploadImage($_FILES['upload_image'], $upload_dir);

        if ($upload_library->file_invalid) {
            exit($this->language->get('account/file_invalid'));
        }
        if ($upload_library->filebig) {
            exit($this->language->get('account/file_big'));
        }

        if ($upload_library->upload_success) {
            $this->log->insertLog('Administrator (' . $user['username'] . ') uploaded a file (' . $upload_library->filename . ') to (' . $upload_dir . ').');
            exit($this->language->get('pages/file_uploaded')); 
        }
    }

    public function drawPageMaker()
    {
        $data['header'] = $this->load->controller('header');
        $data['footer'] = $this->load->controller('footer');
        $data['search'] = $this->load->view('search');
        $data['nav'] = $this->load->view('nav');
        $data['breadcrumb'] = $this->load->controller('breadcrumb');

        exit($this->load->view('new-page', $data));
    }

    public function insertPageData()
    {
        $title = trim($_POST['page_title']);
        $name = trim($_POST['page_name']);
        $route = trim($_POST['page_route']);
        $view = trim($_POST['page_view']);
        $controller = trim($_POST['page_controller']);
        $user = $this->load->model('user')->getUser('id', $this->session->id);

        if (!empty($title)) {
            if (!$this->load->model('pages')->insertPage($title, $name, $route, $view, $controller)) {
                exit('Failed to create page data entry.');
            }
            if (!$this->createPageView($view)) {
                exit('Failed to create page view.');
            }
            if (!$this->createPageController($name, $route, $view, $controller)) {
                exit('Failed to create page controller.');
            }

            $this->log->insertLog('Administrator (' . $user['username'] . ') created a new page. (' . $title . ')');
            exit($this->language->get('pages/page_created'));
        }

        exit($this->language->get('pages/no_name'));
    }

    public function createPageView($view) 
    {
        $view_name = str_replace('.htm', '-page', $view);
        $view = fopen(APPLICATION_DIR . '/views/htm/' . $view, 'w+') or exit('Unable to open view file!');

        fwrite($view, '<?= $header; ?>' . PHP_EOL);
        fwrite($view, '<main class="main ' . $view_name . '">' . PHP_EOL);
        fwrite($view, '    <div class="page-content"><?= $content; ?></div>' . PHP_EOL);
        fwrite($view, '</main>' . PHP_EOL);
        fwrite($view, '<?= $footer; ?>' . PHP_EOL);
        fclose($view);

        return true;
    }

    public function createPageController($name, $route, $view, $controller) 
    {
        $controller_name = str_replace('.php', '', $controller);
        $view_name = str_replace('.htm', '', $view);
        $controller = fopen(APPLICATION_DIR . '/controllers/' . $controller, 'w+') or exit('Unable to open controller file!');

        fwrite($controller, '<?php ' . PHP_EOL);
        fwrite($controller, ' ' . PHP_EOL);
        fwrite($controller, '/**' . PHP_EOL);
        fwrite($controller, ' * ' . $controller_name . ' Controller Class' . PHP_EOL);
        fwrite($controller, ' */' . PHP_EOL);
        fwrite($controller, 'class ' . $controller_name . ' extends Controller' . PHP_EOL);
        fwrite($controller, '{' . PHP_EOL);
        fwrite($controller, '    /**' . PHP_EOL);
        fwrite($controller, '     * Init method' . PHP_EOL);
        fwrite($controller, '     *' . PHP_EOL);
        fwrite($controller, '     * The init method is the default for controller classes. Whenever a controller' . PHP_EOL);
        fwrite($controller, '     * class is instantiated the init method will be called.' . PHP_EOL);
        fwrite($controller, '     * ' . PHP_EOL);
        fwrite($controller, '     * Routes' . PHP_EOL);
        fwrite($controller, '     *  - ' . $route . PHP_EOL);
        fwrite($controller, '     *  - ' . $route . '/init' . PHP_EOL);
        fwrite($controller, '     */' . PHP_EOL);
        fwrite($controller, '    public function init()' . PHP_EOL);
        fwrite($controller, '    {' . PHP_EOL);
        fwrite($controller, '        $data[\'header\'] = $this->load->controller(\'header\');' . PHP_EOL);
        fwrite($controller, '        $data[\'footer\'] = $this->load->controller(\'footer\');' . PHP_EOL);
        fwrite($controller, '        $data[\'content\'] = $this->load->model(\'pages\')->getPageContent(\'' . $name . '\');' . PHP_EOL);
        fwrite($controller, ' ' . PHP_EOL);
        fwrite($controller, '        $this->load->model(\'pages\')->updatePageViews(\'' . $name . '\');' . PHP_EOL);
        fwrite($controller, '        $this->load->model(\'pages\')->updateLastView(\'' . $name . '\');' . PHP_EOL);
        fwrite($controller, ' ' . PHP_EOL);
        fwrite($controller, '        exit($this->load->view(\'' . $view_name . '\', $data));' . PHP_EOL);
        fwrite($controller, '    }' . PHP_EOL);
        fwrite($controller, '}');
        fclose($controller);
        
        return true;
    }

    public function delete() 
    {
        $page_id = $_POST['id'];
        $controllers_dir = APPLICATION_DIR . '/controllers/';
        $views_dir = APPLICATION_DIR . '/views/htm/';
        $page = $this->load->model('pages')->getPageById($page_id);

        if ($page['view_file'] === 'home.htm' || $page['view_file'] === 'index.htm') {
            exit('system_file');
        }

        if (!$this->load->model('pages')->deletePage($page_id)) {
            exit('Unable to delete page data.');
        }

        if (is_file($controllers_dir . $page['controller_file'])) {
            if (!unlink($controllers_dir . $page['controller_file'])) {
                exit('Unable to delete controller file.');
            }
        } 
        if (is_file($views_dir . $page['view_file'])) {
            if (!unlink($views_dir . $page['view_file'])) {
                exit('Unable to delete view file.');
            }
        } 

        exit($this->language->get('pages/page_deleted'));
    }
}