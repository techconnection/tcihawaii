<?php 

/**
 * Login Controller Class
 *
 * The login controller class validates user logins and creates sessions to 
 * track users that have logged in. It also tracks failed login attempts.
 */
class LoginController extends Controller
{
    /**
     * Init method
     *
     * The init methods in controller classes will be called automatically when a 
     * controller is loaded. 
     *
     * Routes
     * - http://phpmvc/acp/login
     * - http://phpmvc/acp/login/init
     */
    public function init()
    {       
        if ($this->session->isLogged()) {
            $this->load->route('/home');
        }

        $data['header'] = $this->load->controller('header');
        $data['footer'] = $this->load->controller('footer');
        $data['sitename'] = $this->load->model('settings')->getSetting('sitename');

        exit($this->load->view('login', $data));
    }

    /**
     * Validate the login form
     *
     * The post data is submitted by ajax in the login view. If the data is 
     * valid this method will also access the session library to create a 
     * session and log the user in.
     */
    public function validate()
    {   
        // Test for bots using the bot test helper.
        $this->helper->botTest($_POST['red_herring']);

        $email = filter_var(trim(strtolower($_POST['email'])), FILTER_SANITIZE_EMAIL);
        $password = $_POST['password'];
        $user_model = $this->load->model('user');
        $user = $user_model->getUser('email', $email);
        $time = date('c');

        if (!$user) {
            exit($this->language->get('login/login_fail'));
        }
        if ($user['group'] == 4) {
            exit($this->language->get('login/locked'));
        }

        // If the user email exists and the password matches. Update the tables and create a session.
        if ($user['email'] && password_verify($password, $user['password'])) {
            $user_model->updateUser('login_attempts', 0, $user['id']);
            $user_model->updateUser('locked_time', null, $user['id']);
            $user_model->updateUser('last_active', $time, $user['id']);
            $user_model->updateUser('ip', $_SERVER['REMOTE_ADDR'], $user['id']);
            $this->session->createSession('id', $user['id']);
            $this->log->insertLog('User (' . $user['username'] . ') logged in.');
            exit('login');
        } else {
            // Increment the login attempt number.
            $attempts = ++$user['login_attempts'];

            // Update the users table with the new login attempt number.
            $user_model->updateUser('login_attempts', $attempts, $user['id']);
            $user_model->updateUser('last_active', $time, $user['id']);

            if ($attempts >= 5 && $attempts <= 10) {
                // If not locked already lock the account
                if (!$user['locked_time']) {
                    if ($user_model->updateUser('locked_time', $time, $user['id'])) {
                        $this->log->insertLog('User (' . $user['username'] . ') account was locked. Too many failed login attempts.');
                    }
                }
                // If it has been less than 5 minutes notify the user they may not log in yet.
                if (time() - strtotime($user['locked_time']) < 5 * 60) {
                    exit($this->language->get('login/locked_5'));
                }
            }
            // After too many failed logins, lock the account.
            if ($attempts >= 10 || $user['group'] == 4) {
                if ($user_model->updateUser('group', 4, $user['id'])) {
                    $this->log->insertLog('User (' . $user['username'] . ') account was locked permanently.');
                    exit($this->language->get('login/locked'));
                }
            }

            // Exit with basic login error if all other checking failed to find a reason.
            $this->log->insertLog('A login attempt was made using the email address (' . $user['email'] . ').');
            exit($this->language->get('login/login_fail'));
        }
    }
}