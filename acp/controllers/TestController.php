<?php 

class TestController extends Controller
{
    public function init()
    { 
        
        // $this->tryCountdown();
    }

    public function checkDay()
    {
        $timestamp = "2017.10.03T13:34";

        $today = new DateTime(); // This object represents current date/time
        $today->setTime( 0, 0, 0 ); // reset time part, to prevent partial comparison

        $match_date = DateTime::createFromFormat( "Y.m.d\\TH:i", $timestamp );
        $match_date->setTime( 0, 0, 0 ); // reset time part, to prevent partial comparison

        $diff = $today->diff( $match_date );
        $diffDays = (integer)$diff->format( "%R%a" ); // Extract days count in interval

        switch( $diffDays ) {
            case 0:
                echo "//Today";
                break;
            case -1:
                echo "//Yesterday";
                break;
            case +1:
                echo "//Tomorrow";
                break;
            default:
                echo "//Sometime";
        }
    }

    public function tryCountdown()
    {
        $date = strtotime('December 25, 2018 12:00 am');
        $remaining = $date - time();
        $days_remaining = floor($remaining / 86400);
        $hours_remaining = floor(($remaining % 86400) / 3600);
        echo 'There are ' . $days_remaining . ' days and ' . $hours_remaining . ' hours left';
    }
}