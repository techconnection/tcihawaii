<?php 

/**
 * Users Controller Class
 *
 * This class gets users information and has the ability to alter a users status.
 */
class UsersController extends Controller
{
    public function init($view = null, $orderby = null, $direction = null, $page = null, $limit = null, $page_total = null)
    {
        $user_model = $this->load->model('user');
        $orderby = $this->load->model('pages')->checkOrderby($orderby);
        $paginated = $this->helper->paginate($page, $limit, $user_model, 'users', $orderby, $direction);
        $total_pages = $paginated['pages'] == 0 ? 'na' : $paginated['pages'];
        
        if ($view) {
            if ($view === 'list') {
                if (!$page || !$limit) {
                    $this->load->route('/acp/users/list/id/asc/' . 1 . '/' . 15);
                }
                if (!$page_total) {
                    $this->load->route('/acp/users/list/' . $orderby . '/' . $direction . '/' . $page . '/' . $limit . '/' . $total_pages);
                }
                $this->drawUserList($paginated);
            } else {
                $this->drawUser($view);
            }
        } else {
            $this->load->route('/acp/users/list');
        }
    }

    private function drawUserList($paginated) 
    {
        $data['header'] = $this->load->controller('header');
        $data['footer'] = $this->load->controller('footer');
        $data['search'] = $this->load->view('search');
        $data['nav'] = $this->load->view('nav');
        $data['breadcrumb'] = $this->load->controller('breadcrumb');

        $users = $paginated['list'];
        if ($users) {
            foreach ($users as $user) {
                if ($this->checkOnline($user['username']) === true) {
                    $is_online = '<span class="text-green">Online</span>';
                } else {
                    $is_online = '<span class="text-red">Offline</span>';
                }
                switch ($user['group']) {
                    case '1':
                        $group = 'Registered';
                        break;
                    case '2':
                        $group = 'Moderator';
                        break;
                    case '3':
                        $group = 'Admin';
                        break;
                    default:
                        $group = 'Activation pending';
                        break;
                }
                $data['users'][] = [
                    'id' => $user['id'],
                    'firstname' => $user['firstname'],
                    'lastname' => $user['lastname'],
                    'username' => $user['username'],
                    'email' => $user['email'],
                    'signup_date' => date('d M, Y', strtotime($user['signup_date'])),
                    'status' => $is_online,
                    'last_active' => date('d M, Y (g:i:s)', strtotime($user['last_active'])),
                    'group' => $group,
                ];
            }
        } else {
            $data['users'] = [];
        }

        $data['list'] = $this->load->view('list-users', $data);
        $data['controls'] = 'user_controls';
        $data['controls'] = $this->load->view('list-controls', $data);

        exit($this->load->view('list', $data));
    }

    private function drawUser($username)
    {
        $data['header'] = $this->load->controller('header');
        $data['footer'] = $this->load->controller('footer');
        $data['search'] = $this->load->view('search');
        $data['nav'] = $this->load->view('nav');
        $data['breadcrumb'] = $this->load->controller('breadcrumb');

        $user = $this->load->model('user')->getUser('username', $username);
        if ($username) {
            $data['id'] = $user['id'];
            if ($user['group'] == 0) { $data['group'] = 'Activation pending'; }
            if ($user['group'] == 1) { $data['group'] = 'Registered'; }
            if ($user['group'] == 2) { $data['group'] = 'Moderator'; }
            if ($user['group'] == 3) { $data['group'] = 'Administrator'; }
            if ($user['group'] == 66) { $data['group'] = 'Locked'; }
            $data['firstname'] = $user['firstname'];
            $data['lastname'] = $user['lastname'];
            $data['username'] = $user['username'];
            $data['email'] = $user['email'];
            $data['registered'] = date('d M, Y', strtotime($user['signup_date']));
            if ($user['privacy'] == 0) { $data['privacy'] = 'Public'; }
            if ($user['privacy'] == 1) { $data['privacy'] = 'Private'; }
            if ($user['privacy'] == 2) { $data['privacy'] = 'Locked'; }
            $data['avatar'] = $user['avatar'];
            $data['bio'] = $user['bio'];

            exit($this->load->view('edit-user', $data));
        }
    }

    public function checkOnline($username) 
    {
        $user = $this->load->model('user')->getUser('username', $username);
        $last_active = strtotime($user['last_active']);

        if (time() - $last_active > 1 * 60) {
            return false;
        } else {
            return true;
        }
    }

    public function editUser()
    {
        if ($this->session->isLogged()) {
            $user_id = $_POST['user_id'];
            $action = $_POST['action'];
            $user_model = $this->load->model('user');
            $user = $user_model->getUser('id', $user_id);
            $admin = $user_model->getUser('id', $this->session->id);
            $group = $user['group'];

            if ($group != 66) {
                if ($action === 'promote' && $group < 3) {
                    $group = ++$group;  
                }
                if ($action === 'demote' && $group > 0) {
                    $group = --$group;
                }
                if ($action === 'lock') {
                    $group = 66;
                }
            }
            if ($action === 'unlock') {
                $group = 1;
            }
            if ($group == 0) {
                $group = 'Activation pending';
                $group_num = '0';
            }
            if ($group == 1) {
                $group = 'Registered';
                $group_num = '1';
            }
            if ($group == 2) {
                $group = 'Moderator';
                $group_num = '2';
            }
            if ($group == 3) {
                $group = 'Administrator';
                $group_num = '3';
            }
            if ($group == 66) {
                $group = 'Locked';
                $group_num = '66';
            }
            
            if ($user_model->updateUser('group', $group_num, $user_id)) {
                $this->log->insertLog('Administrator (' . $admin['username'] . ') changed user (' . $user['username'] . ') group level to (' . $group_num . ', ' . $group . ').');
            }

            $output = ['group' => $group, 'alert' => str_replace('{{group}}', $group, $this->language->get('users/user_updated'))];

            echo json_encode($output);
        }
    }
}