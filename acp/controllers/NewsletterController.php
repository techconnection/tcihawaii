<?php 

/**
 * Subscribers Controller
 *
 * This class gets a list of the sites subscribers. It can also send an 
 * email blast to the subscribers.
 */
class NewsletterController extends Controller
{   
    public function init($view = null, $orderby = null, $direction = null, $page = null, $limit = null, $page_total = null)
    {
        $nl_model = $this->load->model('newsletter');
        $paginated = $this->helper->paginate($page, $limit, $nl_model, 'newsletter', $orderby, $direction);
        $total_pages = $paginated['pages'] == 0 ? 'na' : $paginated['pages'];

        if ($view) {
            if ($view === 'list') {
                if (!$page || !$limit) {
                    $this->load->route('/acp/newsletter/list/id/asc/' . 1 . '/' . 15);
                }
                if (!$page_total) {
                    $this->load->route('/acp/newsletter/list/' . $orderby . '/' . $direction . '/' . $page . '/' . $limit . '/' . $total_pages);
                }
                $this->drawSubscriberList($paginated, $direction, $limit, $orderby);
            }
            if ($view === 'new') {
                $this->edit();
            }
        } else {
            $this->load->route('/acp/newsletter/list');
        }
    }
    
    /*public function init($action = '')
    {
        if ($action == 'new') {
            $this->createEmail();
        } else {
            $newsletter_model = $this->load->model('newsletter');
            $this->sendNewsletter();

            $data['subscribers'] = $newsletter_model->getAll('newsletter');

            $user_model = $this->load->model('user');

            $data['author'] = $this->session->isLogged();
            $data['header'] = $this->load->controller('header');
            $data['footer'] = $this->load->controller('footer');
            $data['search'] = $this->load->view('search');
            $data['nav'] = $this->load->view('nav');
            $data['breadcrumb'] = $this->load->controller('breadcrumb');

            exit($this->load->view('newsletter/list', $data));
        }
    }*/

    public function drawSubscriberList($paginated, $direction, $limit, $orderby) 
    {
        $data['header'] = $this->load->controller('header');
        $data['footer'] = $this->load->controller('footer');
        $data['search'] = $this->load->view('search');
        $data['nav'] = $this->load->view('nav');
        $data['breadcrumb'] = $this->load->controller('breadcrumb');
        $data['author'] = $this->session->isLogged();

        $subscribers = $paginated['list'];
        if ($subscribers) {
            $data['subscribers'] = $subscribers;
        } else {
            $data['subscribers'] = [];
        }

        $data['list'] = $this->load->view('list-newsletter', $data);
        $data['controls'] = 'newsletter_controls';
        $data['controls'] = $this->load->view('list-controls', $data);

        exit($this->load->view('list', $data));
    }

    protected function sendNewsletter()
    {
        if (isset($_POST['send'])) {
            $subject = trim($_POST['subject']);
            $message = trim($_POST['message']);
            $body = '<!DOCTYPE html>
                            <html lang="en">
                                <head>
                                    <meta charset="UTF-8">
                                    <title>'. SITENAME .' Newsletter</title>
                                </head>
                                <body>
                                    <div style="display:block; width:800px; margin:50px auto; text-align:left;">
                                        <a href="http://techsourcehawaii.com" style="float:left; width:100%; background-color:#d20020; padding:13px 21px; text-align:center;"><img src="http://techsourcehawaii.com/app/views/img/logo.png" alt="Tech Source"></a>
                                        ' . $message . '
                                        <div style="float:left; width:100%; background-color:#333; padding:15px 25px; color:#fff; text-align:center;">Tech Connection Inc</div>
                                    </div>
                                </body>
                            </html>';

            $subscribers = $this->subscriber->getAll('newsletter');

            $emails = [];

            foreach ($subscribers as $subscriber) {
                array_push($emails, $subscriber->email);
            }

            $mail_library->sendMail($email_to = $emails, 'DoNotReply', $subject, $body, $message);
            if ($mail_library->send_success) {
                exit($this->language->get('newsletter/newsletter_success'));
            } else {
                exit($this->language->get('newsletter/newsletter_fail'));
            }
        }
    }

    protected function createEmail()
    {
        $data[] = '';
        $data['header'] = $this->load->controller('header');
        $this->load->view('newsletter/new', $data);
        $this->load->controller('footer');
    }
}