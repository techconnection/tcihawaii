<?php 

require_once str_replace('\\', '/', $_SERVER['DOCUMENT_ROOT']) . '/system/config/acp_config.php';

if (is_file(SYSTEM_DIR . '/cache/installed.txt')) {
    require_once SYSTEM_DIR . '/startup.php';
} else {
    require_once SYSTEM_DIR . '/config/install.php';
}