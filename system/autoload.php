<?php 

/**
 * Autoload core class files.
 */
function autoloadCores($class)
{
    $core = CORE_DIR . '/' . $class . '.php';
    if (file_exists($core)) {
        require_once $core;
    }
}

/**
 * Autoload controller class files.
 */
function autoloadControllers($class)
{
    $file = '/' . $class . '.php';
    $controller_paths = iterate(CONTROLLERS_DIR);

    foreach ($controller_paths as $cp) {
        $dirs = explode('/', $cp);
        array_pop($dirs);
        $path = implode('/', $dirs);

        if (is_file($path . $file)) {
            $controller = $path . $file;
            require_once $controller;
        }
    }
}

/**
 * Autoload model class files.
 */
function autoloadModels($class)
{
    $file = '/' . $class . '.php';
    $model_paths = iterate(MODELS_DIR);

    foreach ($model_paths as $mp) {
        $dirs = explode('/', $mp);
        array_pop($dirs);
        $path = implode('/', $dirs);

        if (is_file($path . $file)) {
            $model = $path . $file;
            require_once $model;
        }
    }
}

/**
 * Autoload library class files.
 */
function autoloadLibraries($class)
{
    $lib = LIBRARIES_DIR . '/' . $class . '.php';
    if (file_exists($lib)) {
        require_once $lib;
    }
}

/**
 * Recursively interate dirs
 *
 * This function recursively iterates through a given dir and all its sub dirs.
 * It will return an array of all the files found with their absolute paths.
 * 
 * @param string $source
 * @return array
 */
function iterate($source)
{
    $dir = new RecursiveDirectoryIterator($source);

    foreach (new RecursiveIteratorIterator($dir) as $filename) {
        $files[] = ucfirst($filename->__toString());
    }

    foreach ($files as $file) {
        $file = str_replace('\\', '/', $file);
        $file = explode('/', $file);
        $file = array_diff($file, ['..', '.']);
        $file = implode('/', $file);
        if (is_file($file)) {
            $classes[] = $file;
        }
    }

    return $classes;
}