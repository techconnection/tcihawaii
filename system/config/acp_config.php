<?php 

// Version
define('VERSION', '0.0.0.0');

// Admin Control Panel
define('ACP', true);

// Directories
define('ROOT_DIR', str_replace('\\', '/', $_SERVER['DOCUMENT_ROOT']));
define('HOST', isset($_SERVER['HTTPS']) ? $_SERVER['HTTPS'] . $_SERVER['HTTP_HOST'] : 'http://' . $_SERVER['HTTP_HOST']);
define('APPLICATION_DIR', ROOT_DIR . '/application');
define('MODELS_DIR', ROOT_DIR . '/acp/models');
define('VIEWS_DIR', ROOT_DIR . '/acp/views'); 
define('CONTROLLERS_DIR', ROOT_DIR . '/acp/controllers');
define('LANGUAGE_DIR', ROOT_DIR . '/acp/language');
define('SYSTEM_DIR', ROOT_DIR . '/system');
define('CORE_DIR', ROOT_DIR . '/system/core');
define('LIBRARIES_DIR', ROOT_DIR . '/system/libraries');
define('HELPERS_DIR', ROOT_DIR . '/system/helpers');
define('PLUGINS_DIR', ROOT_DIR . '/system/plugins'); 

// Timezone
date_default_timezone_set('Pacific/Honolulu');

// Errors
error_reporting(E_ALL);
ini_set('display_errors', 'On');

// Database
define('HOSTNAME', 'localhost');
define('DATABASE', 'tcihawaii');
define('USERNAME', 'techutks_chase');
define('PASSWORD', 'Sp@mhol3');