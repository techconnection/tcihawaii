<?php 

/**
 * Loader Class
 *
 * The loader class is used to load the various components of the MVC application. 
 * Pass the file name to the method to load the class. The loading methods will 
 * handle including the necessary files and instantiating the class for that file.
 * The loading methods will also report an error if it cannot include the file or 
 * instantiate the class.
 */
class Loader
{   
    /**
     * Controller loader
     * 
     * Require and instantiate a controller class based on the parameter passed to the method.
     * The parameter should be the controllers parent dir and the controller file.
     * Expected param: account/signup
     * 
     * @param string $controller
     * @return object
     */
    public function controller($controller)
    {   
        if (strpos($controller, '-')) {
            $keys = explode('-', $controller);
            foreach ($keys as $key) {
                $array[] = ucfirst($key);
            }
            $controller = implode($array);
        }

        // Set the first character of the class name to an uppercase letter.
        $class = ucfirst($controller . 'Controller');
        
        // Instantiate the class.
        if (class_exists($class)) {
            $controller = new $class();
        }

        if (is_callable([$controller, 'init'])) {
            return $controller->init();
        } else {
            return $controller;
        }
    }

    /**
     * Model loader
     * 
     * Require and instantiate the model class based on the parameter 
     * passed to the method.
     * 
     * @param string $model
     * @return object
     */
    public function model($model)
    {
        $model = ucfirst($model . 'Model');
        return new $model(); 
    }

    /**
     * View loader
     * 
     * Process the view data for display in the view and require the view file.
     * Exit with notification if file cannot be found or opened.
     * 
     * @param string $view 
     * @param array $data 
     * @param array $raw_data
     */
    public function view($view, $data = [])
    {
        extract($data);
        $file = VIEWS_DIR . '/htm/' . $view . '.htm';
        if (is_file($file)) {
            ob_start();
            require_once $file;
            $content = ob_get_clean();
            return $content;
        }
        exit('The ' . $view . '.htm page does not exist.');
    }

    /**
     * Library loader
     * 
     * Require and instantiate the library class based on the parameter passed to the method.
     * 
     * @param string $library
     * @return object
     */
    public function library($library)
    {
        $library = ucfirst($library);
        return new $library();
    }

    /**
     * Helper loader
     * 
     * Require the helper file if it's a file and exists. Exit with an error if not.
     * 
     * @param string $helper
     */
    public function helper($helper)
    {
        if (is_file(HELPERS_DIR . '/' . $helper . '.php')) {
            require_once HELPERS_DIR . '/' . $helper . '.php';
        } else {
            exit('The ' . $helper . ' helper does not exist.');
        }
    }

    public function route($route)
    {
        exit(header('Location:' . $route));
    }
}