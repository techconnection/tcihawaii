<?php 

/**
 * Application Model
 *
 * The model class is the main model of the application system.
 * All model classes will be extensions of this class.
 */
class Model
{
    public $con;

    public function __construct()
    {
        $this->con = Database::getInstance();
    }

    /**
     * Get All
     *
     * Retrieve all data from a database table and return the results.
     * @return array
     */
    public function getAll($table)
    {
        $array = [];
        $query = $this->con->mysqli->query('SELECT * FROM `' . $table . '`');
        while ($row = $query->fetch_assoc()) {
            $array[] = $row;
        }
        $query->free();
        return $array;
    }

    /** Get total number or records from a given table.
    * 
    *
    */ 
    public function getTotalRecords($table)
    {
        $query = $this->con->mysqli->query('SELECT * FROM `' . $table . '`');
        $num = $query->num_rows;
        return $num;
    }

    /**
     * Get some users
     *
     * @return array
     */
    public function getRecords($table, $orderby, $direction, $start, $limit)
    {
        $array = [];
        $query = $this->con->mysqli->query('SELECT * FROM `' . $table . '` ORDER BY `' . $orderby . '` ' . strtoupper($direction) . ' LIMIT ' . $start . ',' . $limit);
        while ($row = $query->fetch_assoc()) {
            $array[] = $row;
        }
        $query->free();
        return $array;
    }

    public function checkOrderby($orderby)
    {
        switch ($orderby) {
            case 'status':
                return 'last_active';
                break;
            case 'title':
                return 'name';
                break;
            case 'posted':
                return 'date_posted';
                break; 
            case 'order':
                return 'link_order';
                break;  
            default:
                return $orderby;
                break;
        }
    }
}