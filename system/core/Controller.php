<?php 

/**
 * Application Controller
 *
 * The controller class is the main controller of the application system.
 * All controller classes will be extensions of this class.
 */
class Controller 
{   
    /**
     * Load models, views, controllers, helpers, ect.
     * @example $this->load->controller('controllername');
     * @var object
     */
    protected $load = null;

    /**
     * Access the language object to set text for things
     * like page titles and alerts.
     * @var object
     */
    protected $language = null;

    /**
     * Access the sessions library.
     * @var object
     */
    protected $session = null;

    /**
     * Access the log model.
     * @var object
     */
    protected $log = null;

    /**
     * Logged user data.
     * @var object
     */
    protected $logged_user = [];
    
    /**
     * Controller construct
     * 
     * Controller classes are extended form this class so everytime 
     * you load a controller class this construct will be called.
     */
    public function __construct() 
    {
        $this->load = new Loader();
        $this->language = $this->load->library('language');
        $this->session = $this->load->library('session');
        $this->log = $this->load->model('log');
        $this->helper = $this->load->library('helpers');
        $this->url = isset($_GET['url']) ? $this->helper->splitUrl($_GET['url']) : null;

        if (defined('ACP')) {
            $this->requiresAdminLogin();
        }

        // Update users last activity to right now.
        if ($this->session->isLogged()) {
            $this->load->model('user')->updateUser('last_active', date('c'), $this->session->id);
        }
    }

    public function requiresLogin($route = '/login')
    {
        if ($this->session->isLogged()) {
            $this->logged_user = $this->load->model('user')->getUser('id', $this->session->id);
        } else {
            $this->load->route($route);
        }
    }

    public function requiresAdminLogin()
    {
        if ($this->session->isLogged()) {
            $this->logged_user = $this->load->model('user')->getUser('id', $this->session->id);
            if ($this->logged_user['group'] < 3) {
                $this->load->route('/home');
            }
        } else {
            $this->load->route('/home');
        }
    }
}