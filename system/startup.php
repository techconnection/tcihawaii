<?php 

/**
 * Application Startup
 *
 * The router is called by the index.php in the application root. The router uses 
 * the application url to determine the controller, method, and paramters. If no 
 * method is found then the method name in the url becomes a parameter.
 * @example http://root/controller/method/param1/param2
 * @example http://root/account/activate/username/key
 */

// Autoload
require_once SYSTEM_DIR . '/autoload.php';
spl_autoload_register('autoloadCores');
spl_autoload_register('autoloadControllers');
spl_autoload_register('autoloadModels');
spl_autoload_register('autoloadLibraries');

// Start a session if one is not already started
if (!isset($_SESSION)) { session_start(); }

// Default controller.
$controller = 'home';

// Default action/method.
$action = 'init';

// Get the url, sanitize it and create an array with it.
if (isset($_GET['url'])) {
    $url = explode('/', filter_var(trim($_GET['url'], '/'), FILTER_SANITIZE_URL));
}

// Set controller to the first value in the url array.
if (isset($url[0])) {
    $controller = $url[0];
    // Unset the first value in the url array.
    unset($url[0]);
}

if (strpos($controller, '-')) {
    $keys = explode('-', $controller);
    foreach ($keys as $key) {
        $array[] = ucfirst($key);
    }
    $controller = implode($array);
}

// Adjust the class name string to match an actual controller class. Default should be HomeController.
$class = ucfirst($controller) . 'Controller';

// Instantiate the controller class.
if (class_exists($class)) {
    $controller = new $class();
} else {
    if (defined('ACP')) {
        $controller = new OverviewController();
    } else {
        $controller = new NotFoundController();
    }

}

// Set method to the second value in the url array.
if (isset($url[1])) {
    $method = preg_replace('/[^A-Za-z]/', '', $url[1]);
    if (method_exists($controller, $method)) {
        // Set the action to be called to the method name.
        $action = $method;
        // Unset the second value in the url array.
        unset($url[1]);
    }
}

// Create an array of parameters using the pieces of the url. Rebase the keys of the array so the param array keys start at 0.
$params = isset($url) ? array_values($url) : [];

// If the controllers method is callable call the method with params.
if (is_callable([$controller, $action])) {
    call_user_func_array([$controller, $action], $params);
}