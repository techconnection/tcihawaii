<?php

/**
 * Sessions Library Class
 *
 * The session library logs user data based on the session user id and 
 * handles session related data.
 */ 
class Session
{
    /**
     * Users id number
     * @var int
     */
    public $id;

    /**
     * Log a user session.
     * 
     * Check if the user id session is set. If the session is set access
     * the user model user the session user id and get the user data 
     * related to the session.
     * 
     * @return array
     */
    public function isLogged()
    {
        // Check if the session is set.
        if (isset($_SESSION['id'])) { 
            $this->id = $_SESSION['id'];
            return $this->id;
        } else {
            return false;
        }
    }

    /**
     * Create a session.
     * 
     * @param string $session_name 
     * @param mixed $session_value 
     */
    public function createSession($session_name, $session_value)
    {
        if (!isset($_SESSION[$session_name])) {
            $_SESSION[$session_name] = $session_value;
        }
    }

    /**
     * Create a cookie.
     * 
     * @param string $name
     * @param mixed $value
     * @param int $expire 
     */
    public function createCookie($name, $value, $expire)
    {
        setcookie($name, $value, $expire, '/');
    }
}