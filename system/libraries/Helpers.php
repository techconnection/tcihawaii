<?php 

/**
 * 
 */
class Helpers
{
    /**
     * Split URL
     * 
     * Gets the url from the site and explodes it into pieces. Good for when 
     * you want to use the url like an array. website/page/param becomes 
     * array('website', 'page', 'param').
     */
    public function splitUrl($url)
    {
        $url = explode('/', filter_var(trim($url, '/'), FILTER_SANITIZE_URL));
        return $url;
    }

    /**
     * Password Strength Helper
     *
     * If STRONG_PASSWORDS is set to "true" in the settings file then the 
     * checkPasswordStrength function is run. If the password check is set to 
     * run, then users who signup or change their passwords will be forced to 
     * create strong passwords. Strong password requirements are at least 1 
     * upper case, 1 lower case, 1 number and 1 special character.
     */
    public function checkPwStrength($password)
    {
        if (strlen($password) > 8 && preg_match('/[0-9]/', $password) == true && preg_match('/[A-Z]/', $password) == true && preg_match('/[!@#$%]/', $password) == true) {
            return true;
        }
    }

    /**
     * Get a template file to use.
     * 
     * @param string $file 
     * @param string $dir
     * @return string
     */
    public function getTemplate($dir, $file)
    {
        $file = APPLICATION_DIR . '/storage/templates/' . $dir . '/' . $file . '.txt';
        ob_start();
        require ($file);
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
    }

    /**
     * Bot Test Helper
     *
     * Expects the "red_herring" input from forms as a parameter. If the 
     * "red_herring" input is not empty then exit immediately. The assumption 
     * being that only bots would fill out these hidden fields.
     */
    public function botTest($input) 
    {
        if (!empty($input)) {
            exit('AH AH AH. YOU DIDN\'T SAY THE MAGIC WORD!');
        }
    }

    /**
     * Paginate records
     * 
     * @param  [type] $page  [description]
     * @param  [type] $limit [description]
     * @return [type]        [description]
     */
    public function paginate($page = null, $limit = null, $model = null, $table = null, $orderby = null, $direction = null)
    {
        if (isset($page) && isset($limit)) {
            $total = $model->getTotalRecords($table);
            $pages = ceil($total/$limit);
            $start = ($page-1) * $limit;
            $records = $model->getRecords($table, $orderby, $direction, $start, $limit);
            $output = ['pages' => $pages, 'start' => $start, 'list' => $records];
            return $output;
        }
    }
}