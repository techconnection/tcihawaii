<?php 

class Sanitize
{
    /**
     * Escape strings
     * 
     * Sanitize strings passed to this method and return them.
     * 
     * @param string $string
     * @return string
     */
    public function escape($string)
    {
        return htmlentities(trim(strip_tags($string)), ENT_QUOTES, 'UTF-8');
    }

    public function cleanViewData()
    {
        foreach ($data as $d => $value) {
        // If an item in the view data array is a sub array, iterate through the sub array 
        // and sanitize the values then push the data to the data array. If the item is 
        // not a sub array sanitize the data and push it to the data array.
        if (is_array($value)) {
            foreach ($value as $v) {
                if (is_array($v)) {
                    array_walk($v, [$this, 'escape']);
                }
            }
            $data[$d] = $v;
        } else {
            $data[$d] = htmlentities(trim(strip_tags($value)), ENT_QUOTES, 'UTF-8');
        }
    }
}