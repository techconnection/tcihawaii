<?php 

/**
 * Title
 */
$_['title'] = 'Home';

/**
 * Description
 */
$_['description'] = 'This is the home page description and it is about 160 characters long, which is super important for seo or (search engine optimization). Try to keep it so.';

/**
 * Alerts
 */
$_['already_subscribed'] = '<div class="alert error"><strong>Notice</strong> The email address <b>%email%</b> is already subscribed to the newsletter. <button type="button" class="alert-close"><i class="fa fa-times" aria-hidden="true"></i></button></div>';
$_['subscription_inserted'] = '<div class="alert success"><strong>Success!</strong> Thank you for subscribing to our newsletter. You will now receive special notifications and offers. <button type="button" class="alert-continue"><i class="fa fa-times" aria-hidden="true"></i></button></div>';
$_['email_invalid'] = '<div class="alert error"><strong>Error!</strong> The email address "{{email}}" does not appear to be a valid email. <button type="button" class="alert-close"><i class="fa fa-times" aria-hidden="true"></i></button></div>';