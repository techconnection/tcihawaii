<?php 

/**
 * Title
 */
$_['title'] = 'Signup';

/**
 * Description
 */
$_['description'] = 'This is the signup page description and it is about 160 characters long, which is super important for seo or (search engine optimization). Try to keep it so.';

/**
 * Alerts
 */
$_['username_taken'] = '<div class="alert error"><strong>Error!</strong> The username you have chosen is already taken. <button type="button" class="alert-close"><i class="fa fa-times" aria-hidden="true"></i></button></div>';
$_['username_invalid'] = '<div class="alert error"><strong>Error!</strong> Usernames must be letters A-Z and no more than 20 characters long. <button type="button" class="alert-close"><i class="fa fa-times" aria-hidden="true"></i></button></div>';
$_['email_taken'] = '<div class="alert error"><strong>Error!</strong> The email address you have chosen is already taken. <button type="button" class="alert-close"><i class="fa fa-times" aria-hidden="true"></i></button></div>';
$_['email_invalid'] = '<div class="alert error"><strong>Error!</strong> The email address you entered was invalid. <button type="button" class="alert-close"><i class="fa fa-times" aria-hidden="true"></i></button></div>';
$_['pw_weak'] = '<div class="alert error"><strong>Error!</strong> The password is too weak. <br><b>Passwords must:</b> <br>- be 8 or more characters <br>- Have at least 1 number. <br>- Have at least 1 upper case letter. <br>- Contain one of the following (!@#$%). <button type="button" class="alert-close"><i class="fa fa-times" aria-hidden="true"></i></button></div>';
$_['pw_match'] = '<div class="alert error"><strong>Error!</strong> The passwords you entered did not match. <button type="button" class="alert-close"><i class="fa fa-times" aria-hidden="true"></i></button></div>';
$_['signup_success'] = '<div class="alert success"><strong>Success!</strong> Your account has been created and an activation email has been sent to you. <button type="button" class="alert-continue"><i class="fa fa-times" aria-hidden="true"></i></button></div>';
$_['signup_fail'] = '<div class="alert error"><strong>Error!</strong> Unable to create your account. <button type="button" class="alert-close"><i class="fa fa-times" aria-hidden="true"></i></button></div>';
$_['user_banned'] = '<div class="alert error"><strong>Error!</strong> This account has been permanently banned. <button type="button" class="alert-close"><i class="fa fa-times" aria-hidden="true"></i></button></div>';