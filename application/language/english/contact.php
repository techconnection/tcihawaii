<?php 

/**
 * Title
 */
$_['title'] = 'Contact';

/**
 * Description
 */
$_['description'] = 'This is the contact page description and it is about 160 characters long, which is super important for seo or (search engine optimization). Try to keep it so.';

/**
 * Alerts
 */
$_['name_invalid'] = '<div class="alert error"><strong>Error!</strong> Names should be letters A-Z and no more than 20 characters. <button type="button" class="alert-close"><i class="fa fa-times" aria-hidden="true"></i></button></div>';
$_['email_invalid'] = '<div class="alert error"><strong>Error!</strong> The email address you entered was invalid. <button type="button" class="alert-close"><i class="fa fa-times" aria-hidden="true"></i></button></div>';
$_['contact_success'] = '<div class="alert success"><strong>Success!</strong> Thank you for contacting <b>{{sitename}}.</b> Someone will get back to you shortly. <button type="button" class="alert-continue"><i class="fa fa-times" aria-hidden="true"></i></button></div>';
$_['contact_fail'] = '<div class="alert error"><strong>Error!</strong> Your message could not be sent. <button type="button" class="alert-close"><i class="fa fa-times" aria-hidden="true"></i></button></div>';
$_['phone_invalid'] = '<div class="alert error"><strong>Error!</strong> Phone numbers should contain only numeric characters. <button type="button" class="alert-close"><i class="fa fa-times" aria-hidden="true"></i></button></div>';