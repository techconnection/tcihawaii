<?php 

/**
 * Title
 */
$_['title'] = 'Site Map';

/**
 * Description
 */
$_['description'] = 'This is the sitemap page description and it is about 160 characters long, which is super important for seo or (search engine optimization). Try to keep it so.';

/**
 * Alerts
 */