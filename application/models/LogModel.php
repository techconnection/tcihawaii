<?php 

/**
 * Log Model Class
 *
 * Interact with the database to process data related to the log.
 */
class LogModel extends Model
{
    public function insertLog($event)
    {
        $query = $this->con->mysqli->prepare('INSERT INTO log (`event`) VALUES(?)');
        $query->bind_param('s', $event);
        if ($query->execute()) {
            return true;
        } else {
            return false;
        }
    }
}