<?php 

/**
 * Private Message Model Class
 *
 * Interact with the database to process data related to private messages.
 */
class MessageModel extends Model
{
    public function getMessages($param, $data)
    {
        $array = [];
        $query = $this->con->mysqli->query('SELECT * FROM `messages` WHERE `' . $param . '` = "' . $data . '" AND `id` = `chain_id`');
        while ($row = $query->fetch_assoc()) {
            $array[] = $row;
        }
        $query->free();
        return $array;
    }

    public function getMessage($param, $data)
    {
        $array = [];
        $query = $this->con->mysqli->query('SELECT * FROM `messages` WHERE `' . $param . '` = "' . $data . '" AND `chain_id` = `chain_id`');
        while ($row = $query->fetch_assoc()) {
            $array[] = $row;
        }
        $query->free();
        return $array;
    }

    public function getChainId()
    {
        $query = $this->con->mysqli->query('SELECT max(`chain_id`) FROM `messages`');
        $array = $query->fetch_assoc();
        $chain_id = $array['max(`chain_id`)'];
        return $chain_id;
    }

    public function insertMessage($chain_id, $subject, $sender, $receiver, $message)
    {
        $query = $this->con->mysqli->prepare('INSERT INTO `messages` (`chain_id`, `subject`, `sender`, `receiver`, `message`) VALUES(?, ?, ?, ?, ?)');
        $query->bind_param('sssss', $chain_id, $subject, $sender, $receiver, $message);
        if ($query->execute()) {
            return true;
        } else {
            return false;
        }
    }
}