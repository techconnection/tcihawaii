<?php 

/**
 * Settings Model Class
 *
 * Interact with the database to process data related to the settings.
 */
class SettingsModel extends Model
{
    public function getSetting($data)
    {
        $query = $this->con->mysqli->query('SELECT `value` FROM `settings` WHERE `name` = "' . $data . '"');
        $array = $query->fetch_assoc();  
        $value = $array['value'];
        return $value;
    }

    public function updateSetting($row, $param, $id)
    {
        $query = $this->con->mysqli->prepare('UPDATE `settings` SET `' . $row . '` = ? WHERE `id` = ?');
        $query->bind_param('ss', $param, $id);
        if ($query->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function insertAnalyticsCode($code)
    {
        $query = $this->con->mysqli->prepare('INSERT INTO `analytics` (`code`) VALUES(?)');
        $query->bind_param('s', $code);
        if ($query->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function getAnalyticsCode()
    {
        $query = $this->con->mysqli->query('SELECT * FROM `analytics`');
        $array = $query->fetch_assoc();
        return $array;
    }

    public function updateAnalyticsCode($code, $analytics_id = 1)
    {
        $query = $this->con->mysqli->prepare('UPDATE `analytics` SET `analytics_code` = ? WHERE `analytics_id` = ?');
        $query->bind_param('ss', $code, $analytics_id);
        if ($query->execute()) {
            return true;
        } else {
            return false;
        }
    }
}