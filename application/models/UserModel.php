<?php 

/**
 * User Model Class
 *
 * Interact with the database to process data related to the users.
 */
class UserModel extends Model
{
    /**
     * Get a single users data
     *
     * Get a users data and return their data in an array.
     * @param $param
     * @param $data  
     * @return array
     */
    public function getUser($param, $data)
    {
        $query = $this->con->mysqli->query('SELECT * FROM `users` WHERE `' . $param . '` = "' . $data . '"');
        $array = $query->fetch_assoc();
        return $array;
    }

    /**
     * Insert user into database
     *
     * Insert a new user record into the database.
     * @param string $username    
     * @param string $email       
     * @param string $password    
     * @param string $key         
     * @param string $last_active 
     * @param string $ip      
     * @return bool   
     */
    public function insertUser($username, $email, $password, $key, $last_active, $ip)
    {
        $query = $this->con->mysqli->prepare('INSERT INTO `users` (`username`, `email`, `password`, `key`, `last_active`, `ip`) VALUES(?, ?, ?, ?, ?, ?)');
        $query->bind_param('ssssss', $username, $email, $password, $key, $last_active, $ip);
        if ($query->execute()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Update a user record
     *
     * @param string $record 
     * @param mixed $data   
     * @param int $id     
     * @return bool         
     */
    public function updateUser($record, $data, $id)
    {
        $query = $this->con->mysqli->prepare('UPDATE `users` SET `' . $record . '` = ? WHERE `id` = ?');
        $query->bind_param('ss', $data, $id);
        if ($query->execute()) {
            if ($query->affected_rows > 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * Insert recovery link
     *
     * This method inserts a recovery link into the database. It expects a
     * email parameter and a token parameter.
     * @param string $email
     * @param string $token    
     */
    public function insertRecoveryLink($email, $token)
    {
        $query = $this->con->mysqli->prepare('INSERT INTO `reset_links` (`email`, `token`) VALUES(?, ?)');
        $query->bind_param('ss', $email, $token);
        if ($query->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function getRecoveryLink($token)
    {
        $query = $this->con->mysqli->query('SELECT * FROM `reset_links` WHERE `token` = "' . $token . '"'); 
        $array = $query->fetch_assoc();
        return $array;
    }

    public function deleteRecoveryLink($token)
    {
        $query = $this->con->mysqli->prepare('DELETE FROM `reset_links` WHERE `token` = ?');
        $query->bind_param('s', $token);
        $query->execute();
    }
}