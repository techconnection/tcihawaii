<?php 

/**
 * Blog Model Class
 *
 * Interact with the database to process data related to the blog.
 */
class BlogModel extends Model
{
    public function getPosts($limit)
    {
        $array = [];
        $query = $this->con->mysqli->query('SELECT * FROM `blog` ORDER BY `id` DESC LIMIT ' . $limit);
        while ($row = $query->fetch_assoc()) {
            $array[] = $row;
        }
        $query->free();
        return $array;
    }

    public function getPost($param, $data)
    {
        $query = $this->con->mysqli->query('SELECT * FROM `blog` WHERE `' . $param . '` = "' . $data . '"');
        $array = $query->fetch_assoc();
        return $array;
    }

    public function updateBlogViews($id)
    {
        $query = $this->con->mysqli->query('SELECT `views` FROM `blog` WHERE `id` = "' . $id . '"');
        $array = $query->fetch_assoc();
        $current_views = $array['views'];
        $views = ++$current_views;
        $query = $this->con->mysqli->prepare('UPDATE `blog` SET `views` = ? WHERE `id` = ?');
        $query->bind_param('ss', $views, $id);
        if ($query->execute()) {
            if ($query->affected_rows > 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * Update the users last activity.
     *
     * Update the users last active time in their user record.
     * @return bool
     */
    public function updateLastView($id)
    {
        $time = date('c');
        $query = $this->con->mysqli->prepare('UPDATE `blog` SET `last_view` = ? WHERE `id` = ?');
        $query->bind_param('ss', $time, $id);
        if ($query->execute()) {
            if ($query->affected_rows > 0) {
                return true;
            } else {
                return false;
            }
        }
    }
}