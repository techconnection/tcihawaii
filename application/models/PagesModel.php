<?php 

/**
 * Pages Model Class
 *
 * Interact with the database to process data related to pages.
 */
class PagesModel extends Model
{
    public function getPage($data)
    {
        $query = $this->con->mysqli->query('SELECT * FROM `pages` WHERE `name` = "' . $data . '"');
        $array = $query->fetch_assoc();  
        return $array;
    }

    public function getPageContent($data)
    {
        $query = $this->con->mysqli->query('SELECT `content` FROM `pages` WHERE `name` = "' . $data . '"');
        $array = $query->fetch_assoc();
        $content = $array['content'];
        return $content;
    }

    public function updatePageViews($name)
    {
        $query = $this->con->mysqli->query('SELECT `views` FROM `pages` WHERE `name` = "' . $name . '"');
        $array = $query->fetch_assoc();
        $current_views = $array['views'];
        $views = ++$current_views;
        $query = $this->con->mysqli->prepare('UPDATE `pages` SET `views` = ? WHERE `name` = ?');
        $query->bind_param('ss', $views, $name);
        if ($query->execute()) {
            if ($query->affected_rows > 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * Update the users last activity.
     *
     * Update the users last active time in their user record.
     * @return bool
     */
    public function updateLastView($name)
    {
        $time = date('c');
        $query = $this->con->mysqli->prepare('UPDATE `pages` SET `last_view` = ? WHERE `name` = ?');
        $query->bind_param('ss', $time, $name);
        if ($query->execute()) {
            if ($query->affected_rows > 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function eraseSitemap()
    {
        $query = $this->con->mysqli->query('TRUNCATE `sitemap`');
        return $query;
    }

    public function getPageName($data)
    {
        $query = $this->con->mysqli->query('SELECT `name` FROM `pages` WHERE `name` = "' . $data . '"');
        $array = $query->fetch_row();  
        return $array;
    }

    public function insertSitelink($name, $route_name, $route, $link_order)
    {
        $query = $this->con->mysqli->prepare('INSERT INTO `sitemap` (`name`, `route_name`, `route`, `link_order`) VALUES(?, ?, ?, ?)');
        $query->bind_param('ssss', $name, $route_name, $route, $link_order);
        if ($query->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function getSiteLinks()
    {
        $array = [];
        $query = $this->con->mysqli->query('SELECT * FROM `sitemap` ORDER BY `link_order` ASC');
        while ($row = $query->fetch_assoc()) {
            $array[] = $row;
        }
        $query->free();
        return $array;
    }

    public function getSitelink($data)
    {
        $query = $this->con->mysqli->query('SELECT * FROM `sitemap` WHERE `name` = "' . $data . '"');
        $array = $query->fetch_assoc();  
        if ($array != NULL) {
            return true;
        } else {
            return false;
        }
    }
}