<?php 

/**
 * Newsletter Model Class
 *
 * Interact with the database to process data related to the newsletter.
 */
class NewsletterModel extends Model
{
    public function getSubscriber($email)
    {
        $query = $this->con->mysqli->query('SELECT * FROM `newsletter` WHERE `email` = "' . $email . '"');
        $array = $query->fetch_assoc();
        return $array;
    }

    public function insertSubscriber($email)
    {
        $query = $this->con->mysqli->prepare('INSERT INTO `newsletter` (email) VALUES(?)');
        $query->bind_param('s', $email);
        if ($query->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function updateSubscription($record, $data, $email)
    {
        $query = $this->con->mysqli->prepare('UPDATE `newsletter` SET `' . $record . '` = ? WHERE `email` = ?');
        $query->bind_param('ss', $data, $email);
        if ($query->execute()) {
            if ($query->affected_rows > 0) {
                return true;
            } else {
                return false;
            }
        }
    }
}