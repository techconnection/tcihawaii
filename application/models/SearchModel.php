<?php 

/**
 * Search Model Class
 *
 * This class interact with the database and tries to find data that matches 
 * the users search string.
 */
class SearchModel extends Model
{
    public function searchUsers($data)
    {
        return $this->search($this->con->mysqli->query('SELECT * FROM `users` WHERE `username` LIKE "%'. $data . '%" OR `firstname` LIKE "%'. $data . '%" OR `lastname` LIKE "%'. $data . '%" OR `email` LIKE "%'. $data . '%"')); 
    }

    public function searchBlog($data)
    {

       return $this->search($this->con->mysqli->query('SELECT * FROM `blog` WHERE `title` LIKE "%'. $data . '%" OR `author` LIKE "%'. $data . '%"'));
    }

    public function search($query)
    {
        $array = [];
        while ($row = $query->fetch_assoc()) {
            $array[] = $row;
        }
        $query->free();
        return $array;
    }
}