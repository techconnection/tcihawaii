<div class="scructuredcabling-content">
    <div class="row pad-top-large pad-bottom-large bkg-beige">
        <div class="wrapper">
            <div class="row">
                <div class="col big-col">
                    <h1 class="heading-large space-bottom-medium">Structured Cable System</h1>
                    <p class="space-bottom-small text-medium">Structured cabling is the method we use to build our high performance networks. With our system, cables are organized, neat, labeled, racked, terminated, and patched into switches and routers with precision.</p>
                    <p class="space-bottom-small text-medium">Our system has evolved over hundreds of jobs and we have a standard deployment that will work for 15 cables or 400 cables. If your site needs secondary switches in remote locations, our system can handle it with fiber optic backbones or microwave bridges for extremely remote links. You can get started directly on our website using our cable wizard to get a quote. You just tell us how many drops you need and we&#39;ll take you to the right sized rack system.</p>
                </div>
                <div class="col little-col pad-left-large pad-right-large">
                    <img alt="MDF" src="/application/storage/uploads/pages/238080_mdf.png">
                </div>
            </div>
        </div>
    </div>
    <div class="row pad-top-large pad-bottom-large">
        <div class="wrapper">
            <div class="row pad-left-large pad-right-large">
                <div class="row pad-left-large pad-right-large">
                    <h2 class="heading-large space-bottom-medium text-red">All Types of Copper - CAT3, CAT5E, CAT6</h2>
                    <p class="space-bottom-small text-medium">TCI can build your network to any your specification. If you need Cat6 for performance or Cat5e for cost savings, we build the network using a structured system that will keep things simple, clean, and useful.</p>
                    <p class="space-bottom-small text-medium">Why does our system work? Because we follow important guidelines and install the system following our strict guidelines.</p>
                    <ul class="row list-unstyled space-bottom-small pad-left-medium">
                        <li><i class="fas fa-check fa-fw text-red"></i> Cables are attached to hangers and J-Hooks, not left strung out messily in your cieling</li>
                        <li><i class="fas fa-check fa-fw text-red"></i> Cables are run in a &#39;cable highway&#39;, a common pathway through your space that prevents spider-web sprawl</li>
                        <li><i class="fas fa-check fa-fw text-red"></i> Patch panels and cables are labeled and split by type, and even color coded</li>
                        <li><i class="fas fa-check fa-fw text-red"></i> Switch and panels are cleanly integrated by our patch management system</li>
                        <li><i class="fas fa-check fa-fw text-red"></i> Cables are brought out from the ceiling using a dedicated chase pipe - not a ragged hole in your tiles</li>
                        <li><i class="fas fa-check fa-fw text-red"></i> We&#39;ve thought out the little details - the height of wall outlets, the orientation, the color of the jack, etc</li>
                    </ul>
                    <p class="space-bottom-small text-medium">Does your current network adhere to these rules? Wouldn&#39;t you prefer the most important part of your computing operations be the most thought out piece? We see the value in a well done network and our clients do as well.</p>
                </div>
                <hr class="row space-top-medium space-bottom-medium">
                <div class="row pad-left-large pad-right-large">
                    <h3 class="heading-large space-bottom-medium text-red">Fiber Optic Cables</h3>
                    <p class="space-bottom-small text-medium">Our fiber optic laying services are designed as part of our structured system. Our switches are configured to accept fiber cords in the event you need to expand.</p>
                    <p class="space-bottom-small text-medium">We also offer simple as-needed fiber optic services.</p>
                    <div class="col big-col">
                        <ul class="row list-unstyled space-bottom-small pad-left-medium">
                            <li><i class="fas fa-check fa-fw text-red"></i> Cable running and string services for fiber</li>
                            <li><i class="fas fa-check fa-fw text-red"></i> Fiber termination services</li>
                            <li><i class="fas fa-check fa-fw text-red"></i> Fiber splicing services</li>
                            <li><i class="fas fa-check fa-fw text-red"></i> Single mode and multi mode links supported</li>
                        </ul>
                        <p class="space-bottom-small text-medium">In order to terminate these types of cable, we use standard Fujikura fusion splicers that create a precise splice with minimal db loss of .03 or less. Standard Corning optical cable pig tails and patch panels create connectors with low insertion loss. For very high speed links the db loss budget must be kept to a minimum so no manual polishing or mechanical splices are performed.</p>
                        <p class="space-bottom-small text-medium">You can purchase fiber termination services for cables you&#39;ve already run yourself directly on our site. For anything more complicated, you&#39;ll need to call or contact us.</p>
                    </div>
                    <div class="col little-col pad-left-medium pad-right-medium">
                        <img alt="Fusion" src="/application/storage/uploads/pages/Structured Cabling/122724_fusion.jpg">
                    </div>
                </div>
                <hr class="row space-top-medium space-bottom-medium">
                <div class="row pad-bottom-medium">
                    <div class="row space-bottom-medium pad-left-large pad-right-large">
                        <h4 class="heading-large space-bottom-medium text-red">One Day Jobs</h4>
                        <p class="space-bottom-small text-medium">From an IT closet clean-up, to trace and label mapping, to routine MAC work, TCI&#39;s structured cabling brings you a series of solutions that can tame your cabling monsters, all in a single day.</p>
                    </div>
                    <div class="row pad-left-large pad-right-large text-center">
                        <div class="col dual">
                            <div class="row space-bottom-medium">
                                <h5 class="heading-large space-bottom-medium">Cleanups</h5>
                                <img alt="Cleanups" src="/application/storage/uploads/pages/Structured Cabling/129355_cleanups.png">
                            </div>
                            <a class="btn btn-default row" href="/">Purchase Cable Cleanup</a></div>
                            <div class="col dual">
                                <div class="row space-bottom-medium">
                                    <h5 class="heading-large space-bottom-medium">Mapping</h5>
                                    <img alt="Mapping" src="/application/storage/uploads/pages/Structured Cabling/910289_mapping.png">
                                </div>
                                <a class="btn btn-default row" href="/">Purchase Cable Mapping</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>