<div class="homepage-content">
    <div class="row site-greeting text-center">
        <div class="site-greeting-overlay">
            <div class="wrapper">
                <h1 class="text-shadow-red">TECH CONNECTION INC</h1>
                <em>We build high performance networks that anyone can understand and use.</em>
            </div>
        </div>
    </div>
    <div class="row pad-top-medium pad-bottom-medium">
        <div class="wrapper">
            <div class="col quad text-center">
                <a href="/structured-cabling" class="btn-welcome btn-structured-cabling">
                    <span>Structured Cabling</span>
                </a>
            </div>
            <div class="col quad text-center">
                <a href="/network-management" class="btn-welcome btn-network-management">
                    <span>Network Management</span>
                </a>
            </div>
            <div class="col quad text-center">
                <a href="/backup-storage" class="btn-welcome btn-backup-storage">
                    <span>Backup &amp; Storage</span>
                </a>
            </div>
            <div class="col quad text-center">
                <a href="email-spam-protection" class="btn-welcome btn-email-spam">
                    <span>Email &amp; Spam Protection</span>
                </a>
            </div>
            <!-- <div class="col dual">
                <h2 class="heading-medium space-bottom-small text-red"><i class="fas fa-life-ring fa-fw"></i> STRUCTURED CABLING</h2>
                <p class="text-medium pad-bottom-small">Cat5, Cat6, Single-mode and multi-mode fiber optic networks, racked and patched in a properly labeled patch panel with precise cable management. If you are looking to build a network or clean up one that has gotten out of control, our low voltage wiring services are what you need.</p>
                <a href="/structured-cabling" class="btn btn-default">CABLING INFO</a>
            </div>
            <div class="col dual">
                <h3 class="heading-medium space-bottom-small text-red"><i class="fas fa-cubes fa-fw"></i> MANAGED SERVICES</h3>
                <p class="text-medium pad-bottom-small">If you are using one of our networks or have joined our private fiber optic system, we can provide outsourced services like backup, cloud systems, and maintenance and upkeep.  We interface with vendors and contractors so that everything stays structured and simple.</p>
                <a href="/managed-network" class="btn btn-default">SERVICES INFO</a>
            </div> -->
        </div>
    </div>
    <div class="row">
        <div class="wrapper">
            <hr class="row fade">
        </div>
    </div>
    <div class="row pad-top-large pad-bottom-large">
        <div class="wrapper">
            <div class="row pad-left-large pad-right-large">
                <h4 class="heading-medium space-bottom-medium text-center text-red">TCI: SOLVING IT AND COMMUNICATIONS CHALLANGES FOR OVER A DECADE</h4>
                <div class="row pad-left-large pad-right-large">
                    <p class="text-medium pad-bottom-small">At TCI, we take a comprehensive approach to solving your everyday IT problems. No matter how much IT knowledge you have, we can use proven technology to help you. From assessments, to design, to implementation and support, we provide end-to-end IT solutions tailored to your business.</p>
                </div>
                <div class="row pad-left-large pad-right-large">
                    <div class="col dual">
                        <p class="text-medium pad-bottom-small">In addition to our expertise and experience in all industries and with all types of organizations, our partnerships with world-class hardware and software providers like Pure Storage, Ingram Micro, Infoblox, Cisco, HP, EMC, Microsoft, VMware, APC and Asigra allow us to provide businesses of all sizes with sound and reliable data centers, networks and communication platforms.</p>
                    </div>
                    <div class="col dual">
                        <img src="/application/storage/uploads/pages/Home/501532_experts.png" alt="experts" class="experts">
                    </div>
                </div>
                <div class="row pad-left-large pad-right-large">
                    <p class="text-medium pad-bottom-small">Our expert team and unparalleled service have made us the go-to partner for IT solutions and support throughout Texas for over 30 years. Our headquarters are in the Honolulu area.</p>
                </div>
            </div>
            <div class="row pad-top-small pad-bottom-smal text-center">
                <a href="/why-tci" class="btn btn-default">LEARN MORE</a>
            </div>
        </div>
    </div>
</div>