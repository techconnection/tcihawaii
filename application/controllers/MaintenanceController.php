<?php 

/**
 * Maintenance Controller Class
 *
 * The MaintenanceController class is the controller class for the we route to
 * when we are working on the site or doing maintenance and do not want the 
 * public to view the site. 
 */
class MaintenanceController extends Controller
{
    /**
     * Init method
     *
     * The init methods in controller classes will be called automatically when a 
     * controller is loaded. 
     *
     * Routes
     * - http://root/maintenance
     * - http://root/maintenance/init
     *
     * The HomeController class is the default controller for the application. 
     * This means that invalid routes will default to this init method.
     */
    public function init()
    {     
        $settings_model = $this->load->model('settings');
        $maintenance_mode = $settings_model->getSetting('maintenance_mode');
        $sitename = $settings_model->getSetting('sitename');

        if ($maintenance_mode != 1) {
            $this->load->route('/home');
        }

        $data['message'] = str_replace('{{sitename}}', $sitename, $this->language->get('maintenance/maintenance_mode'));

        exit($this->load->view('maintenance', $data));  
    }
}