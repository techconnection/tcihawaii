<?php 

/**
 * Home Controller Class
 *
 * The HomeController class is the default controller class for the application.
 * This is the default controller for the application, so if a route is invalid 
 * or the controller cannot be found or loaded, the user will land here. 
 */
class HomeController extends Controller
{
    /**
     * Init method
     *
     * The init methods in controller classes will be called automatically when a 
     * controller is loaded. 
     *
     * Routes
     * - http://root/home
     * - http://root/home/init
     *
     * The HomeController class is the default controller for the application. 
     * This means that invalid routes will default to this init method.
     */
    public function init()
    {       
        $data['header'] = $this->load->controller('header');
        $data['footer'] = $this->load->controller('footer');
        $data['content'] = $this->load->model('pages')->getPageContent('home');
        $data['popup'] = isset($_COOKIE['newsletter_popup']) ? false : true;

        $this->load->model('pages')->updatePageViews('home');
        $this->load->model('pages')->updateLastView('home');

        // Newsletter
        $this->session->createCookie('newsletter_popup', true, time() + 3600);

        exit($this->load->view('home', $data));  
    }
}