<?php 

/**
 * Newsletter Controller Class
 */
class NewsletterController extends Controller
{
    /**
     * Subscribe to the newsletter.
     * 
     * The method is called by ajax in /application/views/htm/common/home.htm.
     */
    public function subscribe()
    {
        // Validate the email address.
        $email = filter_var(trim(strtolower($_POST['email'])), FILTER_SANITIZE_EMAIL);
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            exit(str_replace('{{email}}', $email, $this->language->get('home/email_invalid')));
        }

        // Load the newsletter model.
        $newsletter_model = $this->load->model('newsletter');

        // Check if the email is already subscribed.
        $subscriber = $newsletter_model->getSubscriber($email);

        // Insert the subscription if there is not already a one with the input email address.
        if (!$subscriber) {
            if ($newsletter_model->insertSubscriber($email)) {
                $this->log->insertLog('A user subscribed to the newsletter using the email address (' . $email . ').');
                exit($this->language->get('home/subscription_inserted'));
            }
        }
    }
}