<?php 

/**
 * Search Controller Class
 *
 * This SearchController class interacts with the SearchModel to find results 
 * in the database matching the search string and pushes them to the view.
 */
class SearchController extends Controller
{
    /**
     * Init method
     *
     * The init methods in controller classes will be called automatically when a 
     * controller is loaded. 
     *
     * Routes
     * - http://root/search
     * - http://root/search/init
     * 
     * @param string $string      
     */
    public function init($string = null)
    {   
        $data['header'] = $this->load->controller('header');
        $data['footer'] = $this->load->controller('footer');
        $data['string'] = $string;

        if ($string) {
            // Load the search model.
            $search_model = $this->load->model('search');
            
            // Search these tables.
            $users = $search_model->searchUsers($string);
            $blog = $search_model->searchBlog($string);

            // Prepare user data for the view.
            if (!empty($users)) {
                foreach ($users as $user) {
                    $data['users'][] = [
                        'username' => $user['username'],
                        'avatar' => $user['avatar'],
                        'country' => $user['country']
                    ];
                }
            }

            // Prepare blog data for the view.
            if (!empty($blog)) {
                foreach ($blog as $post) {
                    $body = preg_replace('/<img[^>]+\>/i', '', $post['body']);
                    $body = preg_replace('/<p[^>]*>[\s|&nbsp;]*<\/p>/', '', $body);
                    $data['blog_posts'][] = [
                        'id' => $post['id'],
                        'author' => $post['author'],
                        'title' => $post['title'],
                        'body' => $body,
                        'preview_image' => $post['preview_image'],
                        'date_posted' => date('d M, Y', strtotime($post['date_posted'])),
                        'blog_link' => strtolower(str_replace(' ', '_', $post['title']))
                    ];
                }
            }
        }

        exit($this->load->view('search', $data));
    }
}