<?php 
 
/**
 * WirelessSolutionsController Controller Class
 */
class WirelessSolutionsController extends Controller
{
    /**
     * Init method
     *
     * The init method is the default for controller classes. Whenever a controller
     * class is instantiated the init method will be called.
     * 
     * Routes
     *  - /wireless-solutions
     *  - /wireless-solutions/init
     */
    public function init()
    {
        $data['header'] = $this->load->controller('header');
        $data['footer'] = $this->load->controller('footer');
        $data['content'] = $this->load->model('pages')->getPageContent('wireless-solutions');
 
        $this->load->model('pages')->updatePageViews('wireless-solutions');
        $this->load->model('pages')->updateLastView('wireless-solutions');
 
        exit($this->load->view('wireless-solutions', $data));
    }
}