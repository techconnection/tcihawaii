<?php 

/**
 * Profile Controller Class
 *
 * This class gets a users data for display on the profile page so it can be 
 * viewed by the public. If the user has marked their profile private, the 
 * public cannot see the profile.
 */
class ProfileController extends Controller
{
    /**
     * Init method
     *
     * The init methods in controller classes will be called automatically when a 
     * controller is loaded. 
     *
     * Routes
     * - http://root/profile
     * - http://root/profile/init
     *
     * The profile init method uses the url username parameter to find a user
     * in the database. If the user is found then the user data is prepared 
     * for the view.
     * 
     * @param string $user
     */
    public function init($user = '')
    {      
        $data['header'] = $this->load->controller('header');
        $data['footer'] = $this->load->controller('footer');
        $data['logged'] = $this->session->isLogged();

        $profile = $this->load->model('user')->getUser('username', $user);

        if ($profile['privacy'] == 1) {
            $data['profile_private'] = 1;
        } else {
            $data['profile_private'] = 0;
        }

        $data['pm'] = str_replace('{{receiver}}', $profile['username'], $this->load->view('profile/pm'));
        $data['id'] = $profile['id'];
        $data['group'] = $profile['group']; 
        $data['firstname'] = $profile['firstname'];
        $data['lastname'] = $profile['lastname'];
        $data['username'] = $profile['username'];
        $data['email'] = $profile['email'];
        $data['registered'] = isset($profile['signup_date']) ? date('D d F, Y', strtotime($profile['signup_date'])) : '';
        $data['avatar'] = $profile['avatar'];
        $data['bio'] = $profile['bio'];
        $data['birthday'] = isset($profile['birthday']) ? date('D d F, Y', strtotime($profile['birthday'])) : '';
        $data['website'] = $profile['website'];
        $data['gender'] = $profile['gender'];
        $country = explode(', ', $profile['country']);
        $data['country'] = isset($country[1]) ? $country[1] : $country[0];

        exit($this->load->view('profile', $data));
    }
}