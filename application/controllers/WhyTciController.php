<?php 
 
/**
 * WhyTciController Controller Class
 */
class WhyTciController extends Controller
{
    /**
     * Init method
     *
     * The init method is the default for controller classes. Whenever a controller
     * class is instantiated the init method will be called.
     * 
     * Routes
     *  - /why-tci
     *  - /why-tci/init
     */
    public function init()
    {
        $data['header'] = $this->load->controller('header');
        $data['footer'] = $this->load->controller('footer');
        $data['content'] = $this->load->model('pages')->getPageContent('why-tci');
 
        $this->load->model('pages')->updatePageViews('why-tci');
        $this->load->model('pages')->updateLastView('why-tci');
 
        exit($this->load->view('why-tci', $data));
    }
}