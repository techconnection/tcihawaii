<?php 
 
/**
 * StructuredCablingController Controller Class
 */
class StructuredCablingController extends Controller
{
    /**
     * Init method
     *
     * The init method is the default for controller classes. Whenever a controller
     * class is instantiated the init method will be called.
     * 
     * Routes
     *  - /structured-cabling
     *  - /structured-cabling/init
     */
    public function init()
    {
        $data['header'] = $this->load->controller('header');
        $data['footer'] = $this->load->controller('footer');
        $data['content'] = $this->load->model('pages')->getPageContent('structured-cabling');
 
        $this->load->model('pages')->updatePageViews('structured-cabling');
        $this->load->model('pages')->updateLastView('structured-cabling');
 
        exit($this->load->view('structured-cabling', $data));
    }
}