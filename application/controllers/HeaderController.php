<?php 

/**
 * Header Controller Class
 *
 * The HeaderController handles logic specific to the header and displays the 
 * header view. The HeaderController should be loaded in each controller class 
 * where a header is desired.
 */
class HeaderController extends Controller
{
    /**
     * Init method
     *
     * The init methods in controller classes will be called automatically when a 
     * controller is loaded. 
     *
     * Assuming that the HeaderController should be loaded in every controller 
     * this init method should run in every controller too.
     */
    public function init()
    {   
        // Update users last activity to right now.
        if ($this->session->isLogged()) {
            $this->load->model('user')->updateUser('last_active', date('c'), $this->session->id);
        }

        $settings_model = $this->load->model('settings');
        $analytics = $settings_model->getAnalyticsCode();

        $data['logged'] = $this->session->isLogged();
        $data['sitename'] = $settings_model->getSetting('sitename');
        $data['analytics'] = $analytics['code'];

        /*$settings_model = $this->load->model('settings');
        if ($settings_model->getSetting('maintenance_mode') == 1) {
            if ($this->session->group <= 1) {
                $this->load->route('/maintenance');
            }
        }*/

        // Split the url into an array.
        $url = isset($_GET['url']) ? $this->helper->splitUrl($_GET['url']) : null;

        $page = $this->load->model('pages')->getPage($url[0]);

        if ($page) {
            $data['title'] = $page['title'];
            $data['description'] = $page['description'];
        } else {
            switch ($url[0]) {
                case '':
                    $this->load->route('/home');
                    break;
                case 'search':
                    $data['title'] = 'Search: ' . isset($url[1]) ? $url[1] : '';
                    $data['description'] = $this->language->get('search/search');
                    break;
                case 'information':
                    if (isset($url[1])) {
                        $data['title'] = $this->language->get($url[1] . '/title');
                        $data['description'] = $this->language->get($url[1] . '/description');
                    } else {
                        $data['title'] = $this->language->get('about/title');
                        $data['description'] = $this->language->get('about/description');
                    }
                    break;
                case 'profile':
                    $data['title'] = isset($url[1]) ? $url[1] : '';
                    $data['description'] = $this->language->get($url[0] . '/description');
                    break;
                default:
                    $data['title'] = $this->language->get($url[0] . '/title');
                    $data['description'] = $this->language->get($url[0] . '/description');
                    break;
            }
        }

        // Return the header view.
        return $this->load->view('header', $data);
    }
}