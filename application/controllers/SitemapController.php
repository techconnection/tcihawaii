<?php 

/**
 * Sitemap Controller Class
 *
 * The sitemap controller is responsible for finding all the links in the host. 
 * It also creates an xml version of the sitemap and prepares the data to be 
 * displayed in the view and saved to the database.
 */
class SitemapController extends Controller
{
    /**
     * Init method
     *
     * The init methods in controller classes will be called automatically when a 
     * controller is loaded. 
     *
     * Routes
     * - http://root/sitemap
     * - http://root/sitemap/init
     * - http://root/information/sitemap
     *
     * This method will access other methods in the class to do a few tasks 
     * related to the sitemap and display the sitemap view.
     */
    public function init()
    {  
        $data['header'] = $this->load->controller('header');
        $data['footer'] = $this->load->controller('footer');
        $data['pages'] = [];

        $sitemap = $this->load->model('pages')->getSiteLinks();

        foreach ($sitemap as $sm) {
            if ($sm['hidden'] == 0) {
                array_push($data['pages'], $sm);
            }
        }

        exit($this->load->view('sitemap', $data));
    }
}