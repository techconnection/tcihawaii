<?php 

/**
 * Information Controller Class
 *
 * This controller class can be used to load different information views 
 * and controllers.
 */
class InformationController extends Controller
{
    /**
     * Init method
     *
     * The init methods in controller classes will be called automatically when a 
     * controller is loaded. 
     * 
     * Routes
     * - http://root/information
     * - http://root/information/init
     * 
     * It doesn't make sense to create a controller for every view, so this class 
     * can load several views depending on the parameter passed. 
     */
    public function init($page = '')
    {
        $data['header'] = $this->load->controller('header');
        $data['footer'] = $this->load->controller('footer');

        // Load the privacy policy view.
        if ($page === 'privacy_policy') {
            // Custom content
            $data['content'] = $this->load->model('pages')->getPageContent('privacy_policy');

            $this->load->model('pages')->updatePageViews('privacy_policy');
            $this->load->model('pages')->updateLastView('privacy_policy');

            exit($this->load->view('privacy', $data));
        }
        // Load the terms & conditions view.
        if ($page === 'terms_conditions') {
            // Custom content
            $data['content'] = $this->load->model('pages')->getPageContent('terms_conditions');

            $this->load->model('pages')->updatePageViews('terms_conditions');
            $this->load->model('pages')->updateLastView('terms_conditions');

            exit($this->load->view('terms', $data));
        }

        // If the action is another information controller
        // route the user to that controller.
        if ($page === 'sitemap') {
            $this->load->route('/sitemap');
        }
        if ($page === 'contact') {
            $this->load->route('/contact');
        }
    }
}