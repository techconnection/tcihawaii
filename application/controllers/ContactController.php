<?php 

/**
 * Contact Controller Class
 *
 * This controller class displays the contact view, which contains the main
 * contact form. This class will also validate the post data and send the 
 * contact email.
 */
class ContactController extends Controller
{
    /**
     * Init method
     *
     * The init method is the default for controller classes. Whenever a controller 
     * class is instantiated the init method will be called.
     * 
     * Routes
     *  - http://root/contact
     *  - http://root/contact/init
     */
    public function init()
    {   
        $data['header'] = $this->load->controller('header');
        $data['footer'] = $this->load->controller('footer');
        $data['content'] = $this->load->model('pages')->getPageContent('contact');

        $this->load->model('pages')->updatePageViews('contact');
        $this->load->model('pages')->updateLastView('contact');

        exit($this->load->view('contact', $data));
    }

    /**
     * Validate the contact form
     * 
     * The post data is submitted by ajax in the contact view. If the 
     * data is valid this method will also access the mail library
     * and send a contact email to the site owner.
     */
    public function validate()
    {

        // Test for bots using the bot test helper.
        $this->load->helper('helpers');
        $this->helper->botTest($_POST['red_herring']);

        // Declare vars for users post data and sanitize the data.
        $firstname = trim($_POST['firstname']);
        if (strlen($firstname) > 20 || preg_match('/[^A-Za-z]/', $firstname)) {
            exit($this->language->get('contact/name_invalid'));
        }

        $email = filter_var(trim(strtolower($_POST['email'])), FILTER_SANITIZE_EMAIL);
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            exit($this->language->get('contact/email_invalid'));
        }

        $sitename = $this->load->model('settings')->getSetting('sitename');

        $subject = $sitename . ' Contact Form';
        $message = trim($_POST['message']);

        // Access the mail library class.
        $mail_library = $this->load->library('mail');

        // Create arrays for finding and replacing placeholders in a mail template.
        $search = ['{{sitename}}', '{{firstname}}', '{{email}}', '{{subject}}', '{{message}}'];
        $replace = [$sitename, $firstname, $email, $subject, $message];

        // Set the body to a template that is retrieved by the mail library and replace 
        // the placeholder text in the template with the matching data using the search
        // and replace arrays.
        $body = str_replace($search, $replace, $mail_library->getTemplate('contact'));

        // Send the contact mail and exit with failure or success message.
        if ($mail_library->sendMail($email_to = ['chase@techsourcehawaii.com'], $email, $subject, $body, $message)) {
            $this->log->insertLog('A user used the contact form with the email address (' . $email . ').');
            exit(str_replace('{{sitename}}', $sitename, $this->language->get('contact/contact_success')));
        } else {
            $this->log->insertLog('A user attempted to use the contact form with the email address (' . $email . ').');
            exit($this->language->get('contact/contact_fail'));
        }
    }
}