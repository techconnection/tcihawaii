<?php 

/**
 * Signup Controller Class
 *
 * This class handles user sign up. It saves post data from the signup form 
 * to the database and also sends the initial activation email.
 */
class SignupController extends Controller
{   
    /**
     * Access the user model.
     * @var object
     */
    private $user_model;

    /**
     * Username from signup form.
     * @var string
     */
    private $username;

    /**
     * Email from signup form.
     * @var string
     */
    private $email;

    /**
     * Hashed password ready for storage.
     * @var string
     */
    private $hashed_password;

    /**
     * Unique key created for each user.
     * @var string
     */
    private $key;

    /**
     * Init method
     *
     * The init methods in controller classes will be called automatically when a 
     * controller is loaded. 
     *
     * Routes
     * - http://phpmvc/signup
     * - http://phpmvc/signup/init
     */
    public function init()
    {      
        $data['sitename'] = $this->load->model('settings')->getSetting('sitename');
        $data['header'] = $this->load->controller('header');
        $data['footer'] = $this->load->controller('footer');

        exit($this->load->view('signup', $data));
    }

    /**
     * Validate the signup form
     * 
     * This is the main validation method and will call all the methods 
     * necessary for the signup to complete.
     */
    public function validate()
    {   
        // Test for bots using the bot test helper.
        $this->helper->botTest($_POST['red_herring']);

        // Load the user model class.
        $this->user_model = $this->load->model('user');

        $this->validateUsername();
        $this->validateEmail();
        $this->validatePassword();
        $this->registerUser();
        $this->sendActivationMail();
    }

    /**
     * Validate username
     *
     * This method will ensure that the username entered by the
     * user is valid by removing any characters that are not a
     * number or in the alphabet. It also checks if the 
     * username is already taken.
     */
    public function validateUsername() 
    {   
        // Remove unwanted characters
        $this->username = preg_replace('/[^A-Za-z0-9_-]/', '', $_POST['username']);
        
        // If username is greater than 20 chars, exit with error.
        if (strlen($this->username) > 20) {
            exit($this->language->get('signup/username_invalid'));
        }

        // If a matching username is found, exit with an error.
        if ($this->user_model->getUser('username', $this->username)) {
            exit($this->language->get('signup/username_taken'));
        }
    }

    /**
     * Validate user email
     * 
     * This method will ensure that the email entered by the user is 
     * valid by using the sanitize email function. It also checks if 
     * the email is already taken.
     */
    public function validateEmail()
    {
        // Sanitize and store the email in a property.
        $this->email = filter_var(trim(strtolower($_POST['email'])), FILTER_SANITIZE_EMAIL);

        // If the email property value does not pass validation exit with an error.
        if (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
            exit($this->language->get('signup/email_invalid'));
        }

        // If a matching email is found, exit with an error.
        if ($this->user_model->getUser('email', $this->email)) {
            exit($this->language->get('signup/email_taken'));
        }
    }

    /**
     * Validate user password
     * 
     * This method is used to check if the passwords entered are the 
     * same. If the strong password setting is on, then it will also 
     * check if the password is too weak. Then finally it will hash 
     * the users password for security.
     */
    public function validatePassword()
    {
        $password = $_POST['password'];
        $confirm = $_POST['confirm'];

        if ($this->load->model('settings')->getSetting('strong_pw')) {
            $pw_strong = checkPwStrength($password);
            if (!$pw_strong) {
                exit($this->language->get('signup/pw_weak'));
            }
        }

        if ($password !== $confirm) {
            exit($this->language->get('signup/pw_match'));
        } 

        $hashed_password = password_hash($password, PASSWORD_BCRYPT, array('cost' => 12));
        $this->hashed_password = $hashed_password;
    }

    /**
     * Register new user
     * 
     * Now we create a unique user key and add the user to database.
     */
    public function registerUser()
    {
        // Create a new unique user key and store it in a property.
        $this->key = md5(mt_rand());

        // Insert the user into the database. If this is the first 
        // user to signup we assume its an admin and we set their 
        // user group to admin level.
        if (!$this->user_model->insertUser($this->username, $this->email, $this->hashed_password, $this->key, time(), $_SERVER['REMOTE_ADDR'])) {
            $this->log->insertLog('A sign up attempted was made using the username (' . $this->username . ') and email address (' . $this->email . ').');
            exit($this->language->get('signup/signup_fail'));
        }
    }

    public function sendActivationMail()
    {
        $sitename = $this->load->model('settings')->getSetting('sitename');
        $mail_library = $this->load->library('mail');
        $subject = 'Activate Your Account';
        $link = 'http://' . HOST . '/account/activate/' . $this->key;
        $body = str_replace('{{link}}', $link, $mail_library->getTemplate('activate'));
        
        if ($mail_library->sendMail($send_to = [$this->email], 'Do Not Reply', $subject, $body, $link)) {
            $mail_library->sendMail($send_to = ['chase@techsourcehawaii.com'], '', 'New User!', str_replace('{{sitename}}', $sitename, $mail_library->getTemplate('signup')), 'new user registered at' . $sitename);
            $this->log->insertLog('A new user (' . $this->username . ') signed up and an activation email has been sent to them at (' . $this->email . ').');
            exit($this->language->get('signup/signup_success'));
        } else {
            $this->log->insertLog('A new user (' . $this->username . ') signed up but an activation email could not be sent to (' . $this->email . ').');
            exit($this->language->get('signup/activate_mail_fail'));
        }
    }
}