<?php 

/**
 * Message Controller Class
 */
class MessagesController extends Controller
{
    public function init($param = 'inbox', $chain_id = null, $subject = null)
    {
        // If the user is not logged in redirect them to the home page.
        if (!$this->session->isLogged()) { $this->load->route('/home'); }

        $data['header'] = $this->load->controller('header');
        $data['footer'] = $this->load->controller('footer');
        $data['type'] = $param;
        $data['subject'] = $subject;

        $messages_model = $this->load->model('message');

        if ($param == 'sent' && $chain_id == null) {
            $data['message'] = null;
            $data['messages'] = $this->processMessages($messages_model->getMessages('sender', $this->logged_user['username']));
        } 

        if ($param == 'inbox' && $chain_id == null) {
            $data['message'] = null;
            $data['messages'] = $this->processMessages($messages_model->getMessages('receiver', $this->logged_user['username']));
        }

        if ($param == 'sent' && $chain_id != null) {
            $data['messages'] = null;
            $data['message'] = $messages_model->getMessage('chain_id', $chain_id);
        }

        if ($param == 'inbox' && $chain_id != null) {
            $data['messages'] = null;
            $data['message'] = $messages_model->getMessage('chain_id', $chain_id);
        }

        exit($this->load->view('messages/messages', $data));
    }

    public function validate($message_type = null)
    {
        if ($this->session->isLogged()) {
            
            $message_model = $this->load->model('message');
            $chain_id = $message_model->getChainId();

            if (!$chain_id) {
                $chain_id = 1;
            } else {
                $chain_id = ++$chain_id;
            }

            $subject = $_POST['subject'];
            $sender = $this->logged_user['username'];
            $receiver = $_POST['receiver'];
            $message = $_POST['message'];

            if ($message_model->insertMessage($chain_id, $subject, $sender, $receiver, $message)) {
                exit('New Message Sent!');
            }
        }  
    }

    private function processMessages($messages)
    {
        foreach ($messages as $m) {
            $arr[] = [
                'id' => $m['id'],
                'chain_id' => $m['chain_id'],
                'subject' => $m['subject'],
                'sender' => $m['sender'],
                'receiver' => $m['receiver'],
                'message' => $m['message'],
                'timestamp' => $m['timestamp'],
                'sender_state' => $m['sender_state'],
                'receiver_state' => $m['receiver_state'],
                'viewed' => $m['viewed'],
                'preview_text' => substr($m['message'], 0, 100)
            ];
        }
        return $arr;
    }
}