<?php 

/**
 * Not Found Controller Class
 *
 * The HomeController class is the default controller class for the application.
 * This is the default controller for the application, so if a route is invalid 
 * or the controller cannot be found or loaded, the user will land here. 
 */
class NotFoundController extends Controller
{
    /**
     * Init method
     *
     * The init methods in controller classes will be called automatically when a 
     * controller is loaded. 
     *
     * Routes
     * - http://root/not_found
     * - http://root/not_found/init
     *
     * The HomeController class is the default controller for the application. 
     * This means that invalid routes will default to this init method.
     */
    public function init()
    {     
        $data['header'] = $this->load->controller('header');
        $data['footer'] = $this->load->controller('footer');

        $url = isset($_GET['url']) ? $this->helper->splitUrl($_GET['url']) : [];
        $data['url'] = $url[0];

        exit($this->load->view('not-found', $data));  
    }
}