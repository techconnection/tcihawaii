<?php 

/**
 * Logout Controller Class
 *
 * The Logout controller will verify logouts and logout inactive users.
 */
class LogoutController extends Controller
{
    /**
     * Init method
     *
     * The init methods in controller classes will be called automatically when a 
     * controller is loaded. 
     *
     * Routes
     * - http://root/logout
     * - http://root/logout/init
     *
     * This init method uses the action parameter to decide what kind of
     * logout is to take place.
     *
     * @param string
     */
    public function init()
    {   
        $data['header'] = $this->load->controller('header');
        $data['footer'] = $this->load->controller('footer');
        $data['inactive'] = false;

        if (!$this->session->isLogged()) {
            $this->load->route('/home');
        }

        $user = $this->load->model('user')->getUser('id', $this->session->id);
        $verify_logout = $this->load->model('settings')->getSetting('verify_logout');

        if (!$verify_logout) {
            if ($this->destroySession()) {
                $this->log->insertLog('User (' . $user['username'] . ') logged out.');
                $this->load->route('/home');
            }
        }

        exit($this->load->view('logout', $data)); 
    }

    public function confirm()
    {
        if ($this->session->isLogged()) {
            $user = $this->load->model('user')->getUser('id', $this->session->id);
        }

        if ($this->destroySession()) {
            $this->log->insertLog('User (' . $user['username'] . ') logged out.');
            $this->load->route('/home');
        }
    }

    public function inactive()
    {
        if ($this->session->isLogged()) {
            $user = $this->load->model('user')->getUser('id', $this->session->id);
        }

        if ($user) {
            if ($this->destroySession()) {
                $this->log->insertLog('User (' . $user['username'] . ') was logged out due to inactivity.');

                $data['header'] = $this->load->controller('header');
                $data['footer'] = $this->load->controller('footer');
                $data['inactive'] = true;

                exit($this->load->view('logout', $data)); 
            }
        } else {
            $this->load->route('/home');
        }
    }

    /**
     * Destroy User Session
     *
     * Destroy the session, logging the user out. After the session 
     * is destroyed redirect them to the homepage. This method will
     * be called by this classes init.
     */
    public function destroySession()
    {
        unset($_SESSION['id']);
        session_destroy();
        return true;
    }
}