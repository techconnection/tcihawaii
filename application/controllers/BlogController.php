<?php 

/**
 * Blog Controller Class
 *
 * This class gets blog data from the database and prepares it for the view.
 */
class BlogController extends Controller
{
    /**
     * Init method
     *
     * The init methods in controller classes will be called automatically when a 
     * controller is loaded. 
     *
     * Routes
     * - http://root/blog
     * - http://root/blog/init
     *
     * This init method uses the link id and link title parameters to get blog
     * data from the database. If the link title is set, the view will show the 
     * post with that title. If the link title is not set, the view will 
     * display the last 5 posts.
     * 
     * @param int $id   
     * @param string $link_title
     */
    public function init($id = null, $link_title = null)
    {       
        $blog_model = $this->load->model('blog');

        $data['header'] = $this->load->controller('header');
        $data['footer'] = $this->load->controller('footer');
        $data['list_view'] = null;

        if (!$link_title) {
            $posts = $blog_model->getPosts(5);
            $data['blog_posts'] = [];
            foreach ($posts as $post) {
                $body = preg_replace('/<img[^>]+\>/i', '', $post['body']);
                $body = preg_replace('/<p[^>]*>[\s|&nbsp;]*<\/p>/', '', $body);
                $data['blog_posts'][] = [
                    'id' => $post['id'],
                    'author' => $post['author'],
                    'title' => $post['title'],
                    'body' => $body,
                    'preview_image' => $post['preview_image'],
                    'date_posted' => date('l, j M, Y', strtotime($post['date_posted'])),
                    'blog_link' => strtolower(str_replace(' ', '_', $post['title'])),
                ];
            }

            $data['list_view'] = true;
        }

        if ($link_title) {
            $blog_data = $blog_model->getPost('id', $id);
            $data['author'] = $blog_data['author'];
            $data['title'] = $blog_data['title'];
            $data['body'] = $blog_data['body'];
            $data['date_posted'] = date('l, j M, Y', strtotime($blog_data['date_posted']));

            $this->load->model('blog')->updateBlogViews($id);
            $this->load->model('blog')->updateLastView($id);
        }

        exit($this->load->view('blog', $data));
    }
}