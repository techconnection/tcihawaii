<?php 

/**
 * Footer Controller Class
 * 
 * The FooterController handles logic specific to the footer and displays the 
 * footer view. The FooterController should be loaded in each controller class 
 * where a footer is desired.
 */
class FooterController extends Controller
{
    /**
     * Init method
     *
     * The init methods in controller classes will be called automatically when a 
     * controller is loaded. 
     * 
     * Assuming that the FooterController should be loaded in every controller 
     * this init method should run in every controller too.
     */
    public function init()
    {     
        return $this->load->view('footer');
    }
}