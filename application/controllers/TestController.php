<?php 

class TestController extends Controller
{
    public function init($test_param = null)
    {     
        $data['header'] = $this->load->controller('header');
        $data['footer'] = $this->load->controller('footer');

        // if ($test_param === 'hrstyles') {
        //     exit($this->load->view('test_hrstyles', $data));
        // }
        // if ($test_param === 'textshadow') {
        //     exit($this->load->view('test_textshadow', $data));
        // }

        // if (!$test_param) {

        //     $var1 = date('D M Y', time());
        //     $var2 = new DateTime();
        //     $var3 = $var2->format('U');

        //     echo $var1 . '<br>' . $var3;

            exit($this->load->view('test', $data));
        // }
    }
}

/**
 * Testing countdown
 */
/*$date = strtotime("December 1, 2017 12:00 AM");
$remaining = $date - time();
$days_remaining = floor($remaining / 86400);
$hours_remaining = floor(($remaining % 86400) / 3600);
echo "There are $days_remaining days and $hours_remaining hours left";*/

/**
 * Testing getting all links from a page
 */
// $html = file_get_contents('http://' . HOST . '/');
// //Create a new DOM document
// $dom = new DOMDocument;

// //Parse the HTML. The @ is used to suppress any parsing errors
// //that will be thrown if the $html string isn't valid XHTML.
// @$dom->loadHTML($html);

// //Get all links. You could also use any other tag name here,
// //like 'img' or 'table', to extract other tags.
// $links = $dom->getElementsByTagName('a');

// //Iterate over the extracted links and display their URLs
// foreach ($links as $link){
//     //Extract and show the "href" attribute.
//     echo $link->nodeValue . '<br>';
//     echo $link->getAttribute('href'), '<br>';
// }