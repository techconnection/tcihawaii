<?php 

/**
 * Account Controller Class
 *
 * This class gets the users data from the database for display on the 
 * account view. It also handles edits to the account that are made by the 
 * account holder, activation and password resets.
 */
class AccountController extends Controller 
{
    /**
    * Username from account form.
    * @var string
    */
    private $username;

    /**
    * Firstname from account form.
    * @var string
    */
    private $firstname;

    /**
    * Lastname from account form.
    * @var string
    */
    private $lastname;

    /**
    * Email from account form.
    * @var string
    */
    private $email;

    /**
    * Access the newsletter model.
    * @var object
    */
    private $newsletter;

    /**
    * Access the mail library.
    * @var object
    */
    private $mail;

    /**
    * Access the user model.
    * @var object
    */
    private $user_model;
    
    /**
     * Init method
     *
     * The init methods in controller classes will be called automatically when a 
     * controller is loaded. 
     *
     * Routes
     * - http://root/account
     * - http://root/account/init
     *
     * This init method gets user data from the database using the user session.
     * This method will also prepare the data for the account view.
     */
    public function init() 
    {  
        // If the user is not logged in redirect them to the home page.
        if (!$this->session->isLogged()) { 
            $this->load->route('/home');
        }

        $data['header'] = $this->load->controller('header');
        $data['footer'] = $this->load->controller('footer');

        if ($this->logged_user['group'] >= 1) {
            $data['activated'] = true;
        } else {
            $data['activated'] = null;
        }

        $data['id'] = $this->logged_user['id'];
        $data['firstname'] = $this->logged_user['firstname'];
        $data['lastname'] = $this->logged_user['lastname'];
        $data['username'] = $this->logged_user['username'];
        $data['email'] = $this->logged_user['email'];
        $data['website'] = $this->logged_user['website'];
        $data['registered'] = $this->logged_user['signup_date'];
        $data['privacy'] = $this->logged_user['privacy'];
        $data['avatar'] = $this->logged_user['avatar'];
        $data['bio'] = $this->logged_user['bio'];
        $data['newsletter'] = $this->getSubscription();

        if ($this->logged_user['group'] == 0) { 
            $data['group'] = 'Un-registered User'; 
        }
        if ($this->logged_user['group'] == 1) { 
            $data['group'] = 'Registered User'; 
        }
        if ($this->logged_user['group'] == 2) { 
            $data['group'] = 'Moderator'; 
        }
        if ($this->logged_user['group'] == 3) { 
            $data['group'] = 'Administrator'; 
        }
        if ($this->logged_user['group'] == 4) { 
            $data['group'] = 'Banned'; 
        }

        if ($this->logged_user['birthday']) {
            $birthday = date('d-F-Y', strtotime($this->logged_user['birthday']));
            $birthday = explode('-', $birthday);
            $data['day'] = $birthday[0];
            $data['month'] = $birthday[1];
            $data['month_num'] = date('m', strtotime($birthday[1]));
            $data['year'] = $birthday[2];
        } else {
            $data['day'] = null;
            $data['month'] = null;
            $data['month_num'] = null;
            $data['year'] = null;
        }

        $countries = $this->load->model('user')->getAll('countries');

        foreach ($countries as $country) {
            $data['countries'][] = [
                'code' => $country['code'],
                'name' => $country['name']
            ];
        }

        if ($this->logged_user['country']) {
            $country = explode(', ', $this->logged_user['country']);
            $data['country_code'] = $country[0];
            $data['country_name'] = $country[1];    
        } else {
            $data['country_code'] = null;
            $data['country_name'] = null;
        }

        $data['gender'] = $this->logged_user['gender'];

        exit($this->load->view('account', $data));
    }

    /**
     * Check subscription
     * 
     * We have to check if the user is already subscribed to the
     * newsletter. So we check the newsletter table for the 
     * email the user is trying to register with. If they are 
     * registered, then update the users table, specifically the
     * newsletter row.
     */
    public function getSubscription()
    {
        // Get a subscriber by searching for the current users session id.
        $subscriber = $this->load->model('newsletter')->getSubscriber('email', $this->logged_user['email']);

        // If the subscriber is found update the users account, specifically their newsletter status.
        if ($subscriber) {
            return $subscriber['status'];
        }
    }
    
    /**
     * Upload Avatar Image
     *
     * This method will be called by the dropzone ajax on the account page.
     * This method is responsible for validating the file and uploading it
     * to the server.
     * 
     * @return bool
     */
    public function uploadAvatar()
    {
        if ($this->session->isLogged()) {
            if (!empty($_FILES['avatar'])) {

                $upload_library = $this->load->library('Upload');
                $image_library = $this->load->library('image');
                $user_model = $this->load->model('user');

                $upload_library->uploadImage($_FILES['avatar'], '/application/storage/uploads/account/');

                if ($upload_library->file_invalid) {
                    exit($this->language->get('account/file_invalid'));
                }

                if ($upload_library->filebig) {
                    exit($this->language->get('account/file_big'));
                }

                if ($upload_library->upload_success) {
                    if (!$user_model->updateUser('avatar', $upload_library->filename, $this->logged_user['id'])) {
                        exit('Couldn\'t update user avatar');
                    }

                    $source = APPLICATION_DIR . '/storage/uploads/account/' . $upload_library->filename;

                    $image_library->cropImage($source, 256, 256, $upload_library->filename, '/application/storage/uploads/account/');

                    if ($image_library->image_cropped) {
                        $this->log->insertLog('User (' . $this->logged_user['username'] . ') uploaded a new avatar (' . $upload_library->filename . ').');
                        exit($this->language->get('account/upload_success'));
                    } else {
                        exit($this->language->get('account/upload_failure'));
                    }
                }
            }
        }
    }

    /**
     * Validate account changes
     *
     * This method validates all changes to the users account. When users hit save
     * on the account page the form is submitted via ajax to this method.
     */
    public function validate() 
    {
        // Dont bother validating anything if the user is not logged in.
        if ($this->session->isLogged()) {

            // Save the user model to a property so we can access it in all of this classes validation methods.
            $this->user_model = $this->load->model('user');

            // Validate all the data coming from the account view.
            $this->validateUsername();
            $this->validateFirstname();
            $this->validateLastname();
            $this->validateEmail();
            $this->validateWebsite();
            $this->validateBirthday();
            $this->validateGender();
            $this->validateCountry();
            $this->validatePrivacy();
            $this->validateNewsletter();
            $this->validatePassword();
            $this->validateBio(); 

            // If none of the validation methods exited with an error. Then the account has updated successfully.
            exit(str_replace('{{username}}', $this->username, $this->language->get('account/account_updated')));
        }
    }

    /**
     * Validate username
     * 
     * Sanitizes and validates the username input record.
     */
    public function validateUsername()
    {
        // We don't want to allow users to emtpy there username field.
        if (!empty($_POST['username'])) {

            // Sanitize the username input.
            $this->username = trim(str_replace(' ', '', preg_replace('/[^A-Za-z0-9]/', '', $_POST['username'])));

            // If the username entered is the same as the logged users username then skip validating the username.
            if ($this->username != $this->logged_user['username']) {

                // Exit if the name is too long.
                if (strlen($this->username) > 20) {
                    exit($this->language->get('account/username_invalid'));
                }

                // Get a users data based on the users input. If no user is found, $user will be null.
                $user = $this->user_model->getUser('username', $this->username);

                // Check if the username entered is already taken.
                // Here we check if $user is falsy. If it is, we assume no user with the entered username exists.
                // If $user is not falsy then it returned a users data. So we compare the found users id with the logged users id.
                // If the ids do not match then the username is taken. If they do we found the logged user and skip ahead.
                if ($user && $user['id'] != $this->logged_user['id']) {
                    exit(str_replace('{{username}}', $this->username, $this->language->get('account/username_taken')));
                }

                // Update the username record or exit with error.
                if ($this->user_model->updateUser('username', $this->username, $this->logged_user['id'])) {
                    $this->log->insertLog('User (' . $this->logged_user['username'] . ') changed their username to (' . $this->username . ').');
                } else {
                    exit($this->language->get('account/username_fail'));
                }
                
            }
        }
    }

    /**
     * Validate firstname
     *
     * Sanitizes and validates the firstname input field.
     */
    public function validateFirstname()
    {
        // Sanitize the firstname input.
        $this->firstname = preg_replace('/[^A-Za-z\-]/', '', trim($_POST['firstname']));

        // If the firstname entered is the same as the logged users firstname then skip validating the firstname.
        if ($this->firstname != $this->logged_user['firstname']) {

            // Exit if the name is too long.
            if (strlen($this->firstname) > 20) { 
                exit($this->language->get('account/name_invalid')); 
            }

            // Update the firstname record or exit with error.
            if ($this->user_model->updateUser('firstname', $this->firstname, $this->logged_user['id'])) {
                $this->log->insertLog('User (' . $this->logged_user['username'] . ') changed their firstname to (' . $this->firstname . ').');
            } else {
                exit('Couldn\'t update first name.');
            }
        }
    }

    /**
     * Validate lastname
     *
     * Sanitizes and validates the lastname input field.
     */
    public function validateLastname()
    {
        // Sanitize the lastname input.
        $this->lastname = preg_replace('/[^A-Za-z\-]/', '', trim($_POST['lastname']));

        // If the lastname entered is the same as the logged users lastname then skip validating the lastname.
        if ($this->lastname != $this->logged_user['lastname']) {

            // Exit if the name is too long.
            if (strlen($this->lastname) > 20) { 
                exit($this->language->get('account/name_invalid')); 
            }

            // Update the lastname record or exit with error.
            if ($this->user_model->updateUser('lastname', $this->lastname, $this->logged_user['id'])) {
                $this->log->insertLog('User (' . $this->logged_user['username'] . ') changed their lastname to (' . $this->lastname . ').');
            } else {
                exit('Couldn\'t update last name.');
            }
        }
    }

    /**
     * Validate email
     *
     * Sanitizes and validates the email input field.
     */
    public function validateEmail()
    {
        // We don't want to allow users to emtpy there email record.
        if (!empty($_POST['email'])) {

            // Sanitize the email input.
            $this->email = filter_var(trim(strtolower($_POST['email'])), FILTER_SANITIZE_EMAIL);

            // If the email entered is the same as the logged users email then skip validating the email.
            if ($this->email != $this->logged_user['email']) {
                if (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
                    exit($this->language->get('account/email_invalid'));
                }

                // Get a users data based on the users input. If no user is found, $user will be null.
                $user = $this->user_model->getUser('email', $this->email);

                // Check if the email entered is already taken.
                // Here we check if $user is falsy. If it is, we assume no user with the entered email exists.
                // If $user is not falsy then it returned a users data. So we compare the found users id with the logged users id.
                // If the ids do not match then the email is taken. If they do we found the logged user and skip ahead.
                if ($user && $user['id'] != $this->logged_user['id']) {
                    exit(str_replace('{{email}}', $this->email, $this->language->get('account/email_taken')));
                } 

                // Update the email record or exit with error.
                if ($this->user_model->updateUser('email', $this->email, $this->logged_user['id'])) {
                    $this->log->insertLog('User (' . $this->logged_user['username'] . ') changed their email to (' . $this->email . ').');
                } else {
                    exit('couldnt update email');
                }
            }
        }
    }

    /**
     * Validate website
     *
     * Sanitizes and validates the website input field.
     */
    public function validateWebsite()
    {
        $website = $_POST['website'];
        if ($website != $this->logged_user['website']) {
            if (preg_match('/^([A-Z0-9-]+\.)+[A-Z]{2,4}$/i', $website) == false) {
                exit($this->language->get('account/website_invalid'));
            }

            if ($this->user_model->updateUser('website', $website, $this->logged_user['id'])) {
                $this->log->insertLog('User (' . $this->logged_user['username'] . ') changed their website to (' . $website . ').');
            } else {
                exit('couldnt update website');
            }
        }
    }

    /**
     * Validate birthday
     *
     * Sanitizes and validates the birthday input field.
     */
    public function validateBirthday()
    {
        $day = $_POST['day'];
        $month = $_POST['month'];
        $year = $_POST['year'];
        $birthday = $year . '-' . $month . '-' . $day;
        if (!empty($day) && !empty($month) && !empty($year)) {
            if ($birthday != $this->logged_user['birthday']) {
                if (!is_numeric($day) && !is_numeric($year)) {
                    exit($this->language->get('account/birthday_invalid'));
                }

                $birthday = $year . '-' . $month . '-' . $day;
                if ($this->user_model->updateUser('birthday', $birthday, $this->logged_user['id'])) {
                    $this->log->insertLog('User (' . $this->logged_user['username'] . ') changed their birthday to (' . $birthday . ').');
                } else {
                    exit('couldnt update birthday');
                }
            }
        }
    }

    public function validateCountry()
    {
        $country = $_POST['country'];
        if ($country != $this->logged_user['country']) {
            if ($this->user_model->updateUser('country', $country, $this->logged_user['id'])) {
                $this->log->insertLog('User (' . $this->logged_user['username'] . ') changed their country to (' . $country . ').');
            } else {
                exit('couldnt update country');
            }
        }  
    }

    public function validateGender()
    {
        $gender = $_POST['gender'];
        if ($gender != $this->logged_user['gender']) {
            if ($this->user_model->updateUser('gender', $gender, $this->logged_user['id'])) {
                $this->log->insertLog('User (' . $this->logged_user['username'] . ') changed their gender to (' . $gender . ').');
            } else {
                exit('couldnt update gender');
            }
        } 
    }

    public function validatePrivacy()
    {
        if (isset($_POST['privacy'])) { 
            $privacy = 1; 
        } else {
            $privacy = 0;
        }

        $status = $privacy == 1 ? '1, Private' : '0, Public';

        if ($privacy != $this->logged_user['privacy']) {
            if ($this->user_model->updateUser('privacy', $privacy, $this->logged_user['id'])) {
                $this->log->insertLog('User (' . $this->logged_user['username'] . ') changed their privacy status to (' . $status . ').');
            } else {
                exit('unable to update privacy');
            }
        }
    }

    public function validateNewsletter()
    {
        if (isset($_POST['newsletter'])) {
            $newsletter = 1;
        } else {
            $newsletter = 0;
        }

        $status = $newsletter == 1 ? '1, On' : '0, Off';
        $newsletter_model = $this->load->model('newsletter');
        $subscriber = $newsletter_model->getSubscriber('email', $this->logged_user['email']);

        if ($subscriber) {
            if ($newsletter == 0) {
                if ($newsletter_model->updateSubscription('status', 0, $this->logged_user['email'])) {
                    $this->log->insertLog('User (' . $this->logged_user['username'] . ') changed their newsletter subscription to (' . $status . ').');
                }
            }
            if ($newsletter == 1) {
                if ($newsletter_model->updateSubscription('status', 1, $this->logged_user['email'])) {
                    $this->log->insertLog('User (' . $this->logged_user['username'] . ') changed their newsletter subscription to (' . $status . ').');
                }
            }
        }

        if (!$subscriber && $newsletter == 1) {
            if ($newsletter_model->insertSubscriber($this->logged_user['email'])) {
                $this->log->insertLog('User (' . $this->logged_user['username'] . ') changed their newsletter subscription to (' . $status . ').');
            }
        }
    }

    public function validatePassword()
    {
        if (!empty($_POST['password']) && !empty($_POST['confirm'])) {
            $password = $_POST['password'];
            $confirm = $_POST['confirm'];
            $password_hash = password_hash($password, PASSWORD_BCRYPT, array('cost' => 12));

            if ($this->load->model('settings')->getSetting('strong_pw')) {
                $pw_strong = $this->helper->checkPwStrength($password);
                if (!$pw_strong) {
                    exit($this->language->get('signup/pw_weak'));
                }
            }

            if ($password != $confirm) {
                exit($this->language->get('account/pw_match'));
            } 

            if ($this->user_model->updateUser('password', $password_hash, $this->logged_user['id'])) {
                unset($_SESSION['id']);
                session_destroy();
                $this->log->insertLog('User (' . $this->logged_user['username'] . ') changed their password using the account page.');
                exit($this->language->get('account/pw_changed'));
            }
        }
        if (!empty($_POST['password']) && empty($_POST['confirm']) || empty($_POST['password']) && !empty($_POST['confirm'])) {
            exit($this->language->get('account/pw_match'));
        }
    }

    public function validateBio()
    {
        $bio = trim($_POST['bio']);
        if ($bio != $this->logged_user['bio']) {
            if ($this->user_model->updateUser('bio', $bio, $this->logged_user['id'])) {
                $this->log->insertLog('User (' . $this->logged_user['username'] . ') changed their bio.');
            } else {
                exit('Couldn\'t update users bio.');
            }
        }
    }

    /**
     * Activate a user account.
     * 
     * Display the activate view. If the key param is not null, check it.
     * If the key is legit then activate the user.
     * @param string $key User key parameter from the url.
     */
    public function activate($key = null)
    {
        $data['header'] = $this->load->controller('header');
        $data['footer'] = $this->load->controller('footer');
        $data['message'] = null;

        if ($key) {
            $user_model = $this->load->model('user');
            $user = $user_model->getUser('key', $key);

            if (!$user) {
                $data['message'] = $this->language->get('account/key_invalid');
            } 

            if ($user_model->updateUser('group', 1, $user['id'])) {
                $this->log->insertLog('User (' . $user['username'] . ') activated their account.');
                $data['message'] = $this->language->get('account/activation_success');
            } else {
                $data['message'] = $this->language->get('account/activation_fail');
                $this->log->insertLog('User (' . $user['username'] . ') attempted to activated their account but failed.');
            }
        }

        

        exit($this->load->view('activate', $data));
    }

    public function sendActivateMail()
    {
        $user_model = $this->load->model('user');
        $user = $user_model->getUser('email', $_POST['email']);

        if ($user) {
            $mail_library = $this->load->library('mail');
            $link = 'http://' . HOST . '/account/activate/' . $user['key'];
            $subject = 'Activate Your Account';
            $body = str_replace('{{link}}', $link, $mail_library->getTemplate('activate'));

            if ($mail_library->sendMail($send_to = [$user['email']], 'Do Not Reply', $subject, $body, $link)) {
                $this->log->insertLog('User (' . $user['username'] . ') sent an activation email to ' . $user['email'] . ').');
                exit($this->language->get('account/activate_mail_sent'));
            } else {
                exit($this->language->get('account/activate_mail_fail'));
            }
        } else {
            exit($this->language->get('account/activate_mail_sent'));
        }
    }

    public function reset($id = null, $token = null)
    {   
        $data['header'] = $this->load->controller('header');
        $data['footer'] = $this->load->controller('footer');
        $data['id'] = $id;
        $data['link_invalid'] = false;
        $data['change_pw'] = false;
        $data['send_email'] = true;

        if ($id && $token) {
            $account_model = $this->load->model('user');
            $link = $account_model->getRecoveryLink($token);
            if ($link) {
                if (time() - strtotime($link['date_created']) > 5 * 60) {
                    $account_model->deleteRecoveryLink($token);
                } 

                $data['change_pw'] = true;
                $data['send_email'] = false;
            } else {
                $data['link_invalid'] = $this->language->get('account/link_invalid');
            }
        }

        exit($this->load->view('reset', $data));
    }

    public function sendRecoveryMail()
    {
        $email = filter_var(trim(strtolower($_POST['email'])), FILTER_SANITIZE_EMAIL);

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            exit(str_replace('{{email}}', $email, $this->language->get('account/email_invalid')));
        }

        $account_model = $this->load->model('user');
        $user = $this->load->model('user')->getUser('email', $email);

        if ($user) {
            $token = bin2hex(openssl_random_pseudo_bytes(16));
            $link = 'http://' . HOST . '/account/reset/' . $user['id'] . '/' . $token;
            $subject = 'Password Reset';
            $mail_library = $this->load->library('mail');
            $body = str_replace('{{link}}', $link, $mail_library->getTemplate('reset'));

            if ($mail_library->sendMail($send_to = [$email], 'Do Not Reply', $subject, $body, $link)) {
                if ($account_model->insertRecoveryLink($email, $token)) {
                    $this->log->insertLog('User (' . $user['username'] . ') reset their password ' . $user['email'] . ') because they forgot it.');
                    exit(str_replace('{{email}}', $email, $this->language->get('account/recovery_sent'))); 
                }
            } else {
                exit(str_replace('{{email}}', $email, $this->language->get('account/recovery_not_sent')));
            }
        }
    }

    public function saveResetPassword()
    {
        if (!empty($_POST['id'])) {
            $id = $_POST['id'];
        }

        if (!empty($_POST['password']) && !empty($_POST['confirm'])) {
            $password = $_POST['password'];
            $confirm = $_POST['confirm'];
            
            if ($this->load->model('settings')->getSetting('strong_pw')) {
                $pw_strong = $this->helper->checkPwStrength($password);
                if (!$pw_strong) {
                    exit($this->language->get('signup/pw_weak'));
                }
            }

            if ($password != $confirm) {
                exit($this->language->get('account/pw_match'));
            } 

            $password_hash = password_hash($password, PASSWORD_BCRYPT, array('cost' => 12));
            $user_model = $this->load->model('user');
            $user = $user_model->getUser('id', $id);

            if ($user_model->updateUser('password', $password_hash, $id)) {
                $this->log->insertLog('User (' . $user['username'] . ') sent an activation email to ' . $user['email'] . ').');
                exit($this->language->get('account/pw_changed'));
            }
        }
        if (!empty($_POST['password']) && empty($_POST['confirm']) ||  empty($_POST['password']) && !empty($_POST['confirm'])) {
            exit($this->language->get('account/pw_match'));
        }
    }

    public function checkUsername()
    {
        if (isset($_POST)) {
            $username = trim(strtoupper($_POST['username']));
            $users = $this->load->model('user')->getAll('users');
            foreach ($users as $user) {
                if ($username == strtoupper($user['username'])) {
                    echo $username;
                }
            }
        }
    }

    public function checkEmail()
    {
        if (isset($_POST)) {
            $email = trim(strtolower($_POST['email']));
            $users = $this->load->model('user')->getAll('users');   
            foreach ($users as $user) {
                if ($email == strtolower($user['email'])) {
                    echo $email;
                }
            }
        }
    }
}