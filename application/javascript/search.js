

$(document).ready(function() {

    $('.search').keypress(function(e) {
        if(e.which == 13){ //Enter key pressed
            return false;
        }
    });

    $('.search').on('keyup', '#search-string', function() {

        if ($('#search-string').val()) {
            $('.search-results').css({'display':'block'});
        } else {
            $('.search-results').css({'display':'none'});
        }

        if ($('#search-string').val().length > 1) {
            
            $.ajax({
                url: '/acp/search/',
                type: 'post',
                data: $('#search-string').serialize(),
                beforeSend: function() {
                    $('.search-results').append(Common.showLoader('../../../application/storage/images/loading.svg'));
                },
                success: function(response) {
                    if (typeof(response) !== 'undefined') {
                        $('.loading').remove();
                        $('.search-results li').remove('');
                        var data = JSON.parse(response);
                        var users = data.users;
                        var posts = data.posts;
                        var pages = data.pages;

                        if (users.length) {
                            $('.search-results-users em').text('Users:');
                            $.each(users, function(index, item) {
                                $('.search-results-users').append('<li><a href="/acp/users/' + item.username + '">' + item.username + '</a></li>');
                            });
                        } else {
                            $('.search-results-users li').remove('');
                            $('.search-results-users em').text('');
                        }

                        if (posts.length) {
                            $('.search-results-blog em').text('Blog:');
                            $.each(posts, function(index, item) {
                                $('.search-results-blog').append('<li><a href="/acp/blog/edit/' + item.id + '">' + item.title + '</a></li>');
                            });
                        } else {
                            $('.search-results-blog li').remove('');
                            $('.search-results-blog em').text('');
                        }

                        if (pages.length) {
                            $('.search-results-pages em').text('Pages:');
                            $.each(pages, function(index, item) {
                                $('.search-results-pages').append('<li><a href="/acp/pages/edit/' + item.name + '">' + item.name + '</a></li>');
                            });
                        } else {
                            $('.search-results-pages li').remove('');
                            $('.search-results-pages em').text('');
                        }
                    }
                }
            });
        }
    });

});