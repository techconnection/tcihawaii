
/**
 * Common Javascript 
 *
 * Common js used site wide. Most of this will be used on every page because
 * a lot of it is for the header and navigation stuff. Dropdown menus, to 
 * top buttons, ect.
 */
var Common = {

    // Drop menus
    dropMenusDown: function() {
        $('.nav ul').on('mouseover', '.dropdown-button', function() {
            if ($(window).width() >= 768) {
                $('.dropdown-menu', this).addClass('active');
                $('i', this).removeClass('fa-caret-down').addClass('fa-caret-up');
            }
        });
        $('.nav ul').on('mouseout', '.dropdown-button', function() {
            if ($(window).width() >= 768) {
                $('.dropdown-menu', this).removeClass('active');
                $('i', this).removeClass('fa-caret-up').addClass('fa-caret-down');
            }
        });  
        $('.nav ul').on('click', '.dropdown-button', function() {
            if ($(window).width() < 768) {
                $('.dropdown-menu', this).toggleClass('active');
                $('i', this).toggleClass('fa-caret-up');
            }
        });
    },

    // Mobile navigation
    openMobileNav: function() {
        $('.header').on('click', '.menu-button', function() {
            $('.nav').toggleClass('active');
        });   
    },

    // Hightlight nav link
    highlightNavLink: function() {
        var pieces = window.location.pathname.split('/');
        var url = pieces[1];
        var nav_class = '.nav-link.' + url;
        $(nav_class).addClass('current');
        switch(url) {
            case 'network-management':
                $('.services').addClass('current');
                break;
            case 'backup-storage':
                $('.services').addClass('current');
                break;
            case 'servers-workstations':
                $('.services').addClass('current');
                break;
            case 'email-spam-protection':
                $('.services').addClass('current');
                break;
            default:
                
        } 
    },

    // Scroll header with page
    scrollHeader: function() {
        $(window).scroll(function() {
            if ($(this).scrollTop() > 100) {
                $('.header-top').css({'height': '50px'});
                $('.logo').css({'margin': '3px 0 0 0'});
            } else {
                $('.header-top').removeAttr('style', '');
                $('.logo').removeAttr('style', '');
            }
        });
    },

    // Go to top button
    goToTop: function() {
        $('.main').append('<div class="to-top"><i class="fa fa-angle-double-up" aria-hidden="true"></i></div>');
        $(window).scroll(function() {
            if ($(this).scrollTop() > 100) {
                $('.to-top').addClass('visible');
            } else {
                $('.to-top').removeClass('visible');
            }
        });
        $('.to-top').click(function() {
            $('html, body').animate( {
                scrollTop: 0
            }, 300);
        });
    },

    search: function() {
        $('.search button').click(function() {
            window.location.replace('/search/' + $('#search-term').val());
        });
        $('.search').keypress(function(e) {
            if (e.which == 13) { //Enter key pressed
                $('.search button').click(); //Trigger search button click event
                return false;
            }
        });
    },

    showLoader: function(image) {
        var loader = '<div class="loading"><div class="loading-spinner"><img src="' + image + '" alt="Loading"></div></div>';
        return loader;
    }
}

$(document).ready(function() {

    Common.dropMenusDown();
    Common.openMobileNav();
    Common.highlightNavLink();
    Common.scrollHeader();
    Common.goToTop();
    Common.search();

    $('body').on('click', '.alert-continue', function() {
        window.location.reload();
    });
    $('body').on('click', '.alert-close', function() {
        $(this).parent().remove();
    });
    $('body').on('click', '.alert-redirect', function() {
        window.location.replace('/home');
    });

});