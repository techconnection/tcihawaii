
/**
 * Common Javascript 
 *
 * Common js used site wide. Most of this will be used on every page because
 * a lot of it is for the header and navigation stuff. Dropdown menus, to 
 * top buttons, ect.
 */
var Common = {

    // Drop menus
    dropMenusDown: function() {
        $('.p-nav ul').on('mouseover', '.dropdown-button', function() {
            if ($(window).width() >= 768) {
                $('.dropdown-menu', this).addClass('active');
                $('i', this).removeClass('fa-caret-down').addClass('fa-caret-up');
            }
        });
        $('.p-nav ul').on('mouseout', '.dropdown-button', function() {
            if ($(window).width() >= 768) {
                $('.dropdown-menu', this).removeClass('active');
                $('i', this).removeClass('fa-caret-up').addClass('fa-caret-down');
            }
        });
        $('.p-nav ul a').on('click', '.dropdown-button', function() {
            return false;
        });  
        $('.p-nav ul').on('click', '.dropdown-button', function() {
            if ($(window).width() < 768) {
                $('.dropdown-menu', this).toggleClass('active');
                $('i', this).toggleClass('fa-caret-up');
            }
        });
    },

    // Mobile navigation
    openMobileNav: function() {
        $('.header').on('click', '.menu-button', function() {
            $('.p-nav').toggleClass('active');
        });   
    },

    // Hightlight nav link
    highlightNavLink: function() {
        var pieces = window.location.pathname.split('/');
        var url = pieces[2];
        var nav_class = '.p-nav-link.' + url;
        $(nav_class).addClass('current');
    },

    search: function() {
        $('.search button').click(function() {
            window.location.replace('/acp/search/' + $('#search-term').val());
        });
        $('.search').keypress(function(e) {
            if (e.which == 13) { //Enter key pressed
                $('.search button').click(); //Trigger search button click event
                return false;
            }
        });
    },

    showLoader: function(image) {
        var loader = '<div class="loading"><div class="loading-spinner"><img src="' + image + '" alt="Loading"></div></div>';
        return loader;
    },

    goBack: function() {
        $('body').on('click', '.btn-goback', function() { 
            window.history.back(); 
        });
    },

    animateInputs: function() {
        $('.fancy-form label').each(function() {
            if ($(this).next().val() != '') {
                $(this).css({'font-size': '10px'});
            }
        });  
        $('.fancy-form input').each(function() {
            if ($(this).val() != '') {
                $(this).css({'height' : '55px', 'padding-top' : '18px'});
            }
        });
        $('.fancy-form textarea').each(function() {
            if ($(this).val() != '') {
                $(this).css({'padding-top' : '25px'});
            }
        });  
        $('form').on('click', 'label', function() {
            $(this).next('input').focus();
            $(this).next('textarea').focus();
        });
        $('.fancy-form').on('focus', '.fancy-row', function() {
            $('input', this).css({'height' : '55px', 'padding-top' : '18px'});
            $('textarea', this).css({'padding-top' : '25px'});
            $('label', this).css({'font-size' : '11px', 'display': 'block', 'color' : '#777'});
        });
        $('.fancy-form').on('blur', '.fancy-row', function() {
            if (!$('input, textarea', this).val()) {
                $('input', this).removeAttr('style');
                $('label', this).css({'font-size' : '13px', 'color' : '#bbb'});
            }
        });
    },

    getThemeSetting: function() {
        $.ajax({
            url: '/acp/settings/getThemeSetting',
            type: 'get',
            success: function(response) {
                if (typeof(response) !== 'undefined') {
                    if (response == 0) {
                        $('body').removeClass('dark-theme').addClass('light-theme');
                        $('.btn-theme').text(' Theme').prepend('<i class="fas fa-sun fa-fw"></i>');
                    }
                    if (response == 1) {
                        $('body').removeClass('light-theme').addClass('dark-theme');
                        $('.btn-theme').text(' Theme').prepend('<i class="fas fa-moon fa-fw"></i>');
                    }
                }
            }
        });
    },

    changeThemeSetting: function() {
        $('.account-nav').on('click', '.btn-theme', function() {

            $.ajax({
                url: '/acp/settings/changeThemeSetting',
                type: 'get',
                beforeSend: function() {
                    $('.btn-theme').text(' Loading...').prepend('<i class="fas fa-spinner fa-pulse fa-fw"></i>');
                },
                success: function(response) {
                    if (typeof(response) !== 'undefined') {
                        if (response == 0) {
                            $('body').removeClass('dark-theme').addClass('light-theme');
                            $('.btn-theme').text(' Theme').prepend('<i class="fas fa-sun fa-fw"></i>');
                        }
                        if (response == 1) {
                            $('body').removeClass('light-theme').addClass('dark-theme');
                            $('.btn-theme').text(' Theme').prepend('<i class="fas fa-moon fa-fw"></i>');
                        }
                    }
                }
            });

        });
    },

    getMenuSetting: function() {
        $.ajax({
            url: '/acp/settings/getMenuSetting',
            type: 'get',
            success: function(response) {
                if (typeof(response) !== 'undefined') {
                    if (response == 0) {
                        if ($('.p-nav').hasClass('p-menu-open') == true) {
                            $('.p-nav').removeClass('p-menu-open');
                            $('.p-workarea').removeClass('p-menu-open');
                            $('.btn-menu i').removeClass('fa-toggle-on').addClass('fa-toggle-off');
                        }
                    }
                    if (response == 1) {
                        if ($('.p-nav').hasClass('p-menu-open') == false) {
                            $('.p-nav').addClass('p-menu-open');
                            $('.p-workarea').addClass('p-menu-open');
                            $('.btn-menu i').addClass('fa-toggle-on').removeClass('fa-toggle-off');
                        }
                    }
                }
            }
        });
    },

    changeMenuSetting: function() {
        $('.p-main-nav').on('click', '.btn-menu', function(e) {
            e.preventDefault();
            $.ajax({
                url: '/acp/settings/changeMenuSetting',
                type: 'get',
                success: function(response) {
                    if (typeof(response) !== 'undefined') {
                        if (response == 0) {
                            $('.btn-menu i').removeClass('fa-toggle-on').addClass('fa-toggle-off');
                            $('.p-nav').removeClass('p-menu-open');
                            $('.p-workarea').removeClass('p-menu-open');
                        }
                        if (response == 1) {
                            $('.btn-menu i').addClass('fa-toggle-on').removeClass('fa-toggle-off');
                            $('.p-nav').addClass('p-menu-open');
                            $('.p-workarea').addClass('p-menu-open');
                        }
                    }
                }
            });
            $('.p-nav').toggleClass('p-menu-open');
            $('.btn-menu i').toggleClass('fa-toggle-on').toggleClass('fa-toggle-off');
            $('.p-workarea').toggleClass('p-menu-open');
        });
    }
}

$(document).ready(function() {

    Common.dropMenusDown();
    Common.openMobileNav();
    Common.highlightNavLink();
    Common.search();
    Common.goBack();
    Common.animateInputs();
    Common.getThemeSetting();
    Common.getMenuSetting();
    Common.changeThemeSetting();
    Common.changeMenuSetting();

    $('body').on('click', '.alert-continue', function() {
        window.location.reload();
    });
    $('body').on('click', '.alert-close', function() {
        $(this).parent().remove();
    });
    $('body').on('click', '.alert-redirect', function() {
        window.location.replace('/acp/overview');
    });

    $(document).on('click', '.btn', function() {
        $(this).addClass('disabled').html('<i class="fas fa-spinner fa-pulse fa-fw"></i> Loading...');
        $('.alert').remove();
    });

});

$(window).on('load', function() {
    $('.loading').fadeOut(1000);
});